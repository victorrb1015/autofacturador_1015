<!-- Sidebar navigation -->
<div id="slide-out" class="side-nav wide sn-bg-1">
    <ul class="custom-scrollbar">
        <!-- Logo -->
        <li>
            <div class="logo-wrapper sn-ad-avatar-wrapper black">
                <a href="{{route('home')}}"><img src="{{asset('img/generic-logo.png')}}" class="rounded-circle" alt=""><span>Central</span></a>
            </div>
        </li>
        <!--/. Logo -->
        <!-- Side navigation links -->
        <li>
            <ul class="collapsible collapsible-accordion">
                <li>
                    <a class="collapsible-header waves-effect arrow-r black-text">
                        <i class="sv-slim-icon fas fa-chart-area"></i> Dashboard
                        <i class="fas fa-angle-down rotate-icon"></i>
                    </a>
                    <div class="collapsible-body">
                        <ul>
                            <li><a href="{{route('dashboard')}}" class="waves-effect active black-text">
                                    <span class="sv-slim"> DB </span>
                                    <span class="sv-normal">Dashboard</span></a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <a class="collapsible-header waves-effect arrow-r black-text">
                        <i class="sv-slim-icon fas fa-clipboard-list"></i> Facturas
                        <i class="fas fa-angle-down rotate-icon"></i>
                    </a>
                    <div class="collapsible-body">
                        <ul>
                            <li><a href="{{route('facturas')}}" class="waves-effect black-text">
                                    <span class="sv-slim"> FA </span>
                                    <span class="sv-normal">Facturas</span></a>
                            </li>
                            <li><a href="{{route('facturas.new')}}" class="waves-effect black-text">
                                    <span class="sv-slim"> GF </span>
                                    <span class="sv-normal">Generar Factura</span></a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <a class="collapsible-header waves-effect arrow-r black-text">
                        <i class="sv-slim-icon fas fa-ticket-alt"></i> Tickets
                        <i class="fas fa-angle-down rotate-icon"></i>
                    </a>
                    <div class="collapsible-body">
                        <ul>
                            <li><a href="{{route('tickets')}}" class="waves-effect black-text">
                                    <span class="sv-slim"> TK </span>
                                    <span class="sv-normal">Tickets</span></a>
                            </li>
                            <li><a href="{{route('validar')}}" class="waves-effect black-text">
                                    <span class="sv-slim"> VT </span>
                                    <span class="sv-normal">Validar Tickets</span></a>
                            </li>
                            <li><a href="{{route('sin_est')}}" class="waves-effect black-text">
                                    <span class="sv-slim"> TS </span>
                                    <span class="sv-normal">Tickets sin estacionamiento</span></a>
                            </li>
                            <li><a href="{{route('rechazada')}}" class="waves-effect black-text">
                                    <span class="sv-slim"> R </span>
                                    <span class="sv-normal">Rechazos</span></a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <a class="collapsible-header waves-effect arrow-r black-text">
                        <i class="sv-slim-icon fas fa-parking"></i> Estacionamiento
                        <i class="fas fa-angle-down rotate-icon"></i>
                    </a>
                    <div class="collapsible-body">
                        <ul>
                            <li><a href="{{route('park')}}" class="waves-effect active black-text">
                                    <span class="sv-slim"> LS </span>
                                    <span class="sv-normal">Lista</span></a>
                            </li>
                            <li><a href="{{route('park.new')}}" class="waves-effect active black-text">
                                    <span class="sv-slim"> NE </span>
                                    <span class="sv-normal">Nuevo Estacionamiento</span></a>
                            </li>
                        </ul>
                    </div>
                </li>
                <!--<li>
                    <a class="collapsible-header waves-effect arrow-r black-text">
                        <i class="sv-slim-icon fas fa-address-card"></i> Pensiones
                        <i class="fas fa-angle-down rotate-icon"></i>
                    </a>
                    <div class="collapsible-body">
                        <ul>
                            <li><a href="{{route('pen')}}" class="waves-effect active black-text">
                                    <span class="sv-slim"> LS </span>
                                    <span class="sv-normal">Lista</span></a>
                            </li>
                        </ul>
                    </div>
                </li>-->
                <li>
                    <a class="collapsible-header waves-effect arrow-r black-text">
                        <i class="sv-slim-icon fas fa-user-circle"></i> Usuarios
                        <i class="fas fa-angle-down rotate-icon"></i>
                    </a>
                    <div class="collapsible-body">
                        <ul>
                            <li><a href="{{route('lista')}}" class="waves-effect active black-text">
                                    <span class="sv-slim"> US </span>
                                    <span class="sv-normal">Usuarios</span></a>
                            </li>
                            <li><a href="{{ route('reg') }}" class="waves-effect active black-text">
                                    <span class="sv-slim"> NU </span>
                                    <span class="sv-normal">Nuevo Usuario</span></a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li>
                    <strong class="black-text">
                        <a id="toggle" class="waves-effect">
                            <i class="sv-slim-icon fas fa-angle-double-left"></i><strong class="black-text">Minimiza el menú</strong>
                        </a>
                    </strong>
                </li>
            </ul>
        </li>
        <!--/. Side navigation links -->
    </ul>
    <div class="sidenav-bg fixed "></div>
</div>
<!--/. Sidebar navigation -->
