@include('layouts.sidenav')
<!-- Navbar -->

<nav class="navbar fixed navbar-toggleable-md navbar-expand-lg scrolling-navbar double-nav">
    <!-- Breadcrumb-->
    <div class="breadcrumb-dn mr-auto">
        <p><img src="{{asset('img/generic-logo.png')}}"
                @auth
                data-activates="slide-out" class="button-collapse" id="botoncollapse"
                @endauth
                height="30" alt="mdb logo"></p>
    </div>
    <ul class="nav navbar-nav nav-flex-icons ml-auto">
        <li class="nav-item">
            <a class="btn-floating btn-sm central material-tooltip-email" data-toggle="tooltip" href="#"
               title="{{ __('Soporte') }}"><i class="far fa-comments"></i></a>
        </li>
        @guest

        @else
            <li class="nav-item">
                <a class=" btn-floating btn-sm central material-tooltip-email" data-toggle="tooltip" href="#"
                   title="{{ Auth::user()->name }}"><i class="far fa-user-circle"></i></a>
            </li>
            <li class="nav-item">
                <a class="btn-floating btn-sm central material-tooltip-email" type="#logout-form" onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();" title="{{ __('Logout') }}">
                    <i class="fas fa-sign-out-alt"></i>
                </a>
            </li>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        @endguest
    </ul>
</nav>
