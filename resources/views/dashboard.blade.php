@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-6">
                        <!-- Card -->
                        <div class="card">
                            <!-- Card Data -->
                            <div class="row mt-3">
                                <div class="col-md-5 col-5 text-left pl-4">
                                    <a type="button" class="btn-floating btn-lg light-green darken-3 ml-4"
                                       href="{{route('tickets')}}"><i class="fas fa-ticket-alt" aria-hidden="true"></i></a>
                                </div>
                                <div class="col-md-7 col-7 text-right pr-5">
                                    <h5 class="ml-4 mt-4 mb-2 font-weight-bold">{{$total}}</h5>
                                    <p class="font-small grey-text">Tickets del día</p>
                                </div>
                            </div>
                            <!-- Card Data -->
                        </div>
                        <!-- Card -->
                        <br>
                    </div>

                    <div class="col-md-6">
                        <!-- Card -->
                        <div class="card">
                            <!-- Card Data -->
                            <div class="row mt-3">
                                <div class="col-md-5 col-5 text-left pl-4">
                                    <a type="button" class="btn-floating btn-lg light-green darken-3 lighten-1 ml-4"><i
                                            class="fas fa-dollar-sign" aria-hidden="true"></i></a>
                                </div>
                                <div class="col-md-7 col-7 text-right pr-5">
                                    <h5 class="ml-4 mt-4 mb-2 font-weight-bold">{{$facturado}}</h5>
                                    <p class="font-small grey-text">Facturado</p>
                                </div>
                            </div>
                            <!-- Card Data -->
                        </div>
                        <!-- Card -->
                        <br>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <!-- Card -->
                        <div class="card">
                            <!-- Card Data -->
                            <div class="row mt-3">
                                <div class="col-md-5 col-5 text-left pl-4">
                                    <a type="button" class="btn-floating btn-lg light-green darken-3 ml-4"><i
                                            class="fas fa-user"
                                            aria-hidden="true"></i></a>
                                </div>
                                <div class="col-md-7 col-7 text-right pr-5">
                                    <h5 class="ml-4 mt-4 mb-2 font-weight-bold">{{$fact}}</h5>
                                    <p class="font-small grey-text">Facturas del día</p>
                                </div>
                            </div>
                            <!-- Card Data -->
                        </div>
                        <!-- Card -->
                        <br>
                    </div>
                    <br>
                    <div class="col-md-6">
                        <!-- Card -->
                        <div class="card">
                            <!-- Card Data -->
                            <div class="row mt-3">
                                <div class="col-md-5 col-5 text-left pl-4">
                                    <a type="button" class="btn-floating btn-lg light-green darken-3 ml-4"><i
                                            class="fas fa-exclamation-circle" aria-hidden="true"></i></a>
                                </div>
                                <div class="col-md-7 col-7 text-right pr-5">
                                    <h5 class="ml-4 mt-4 mb-2 font-weight-bold">{{ $rechazo }}</h5>
                                    <p class="font-small grey-text">Rechazadas</p>
                                </div>
                            </div>
                            <!-- Card Data -->
                        </div>
                        <!-- Card -->
                        <br>
                    </div>
                </div>
            </div>
            <br>
            <br>
            <div class="col-md-6">
                <div class="card card-cascade wider reverse">

                    <!-- Card image -->
                    <div class="view view-cascade overlay">
                        <canvas id="pieChart" class="card-img-top" alt="Card image cap"></canvas>
                    </div>
                    <!-- Card content -->
                    <div class="card-body card-body-cascade text-center central">
                        <!-- Title -->
                        <h4 class="card-title"><strong class="white-text">Facturas</strong></h4>
                    </div>
                </div>
            </div>
        </div>
        <div class="row py-4">
            <div class="col-md-8">
                <!-- Card -->
                <div class="card card-cascade wider col-md-12">
                    <div class="view view-cascade gradient-card-header central">
                        <h2 class="card-header-title mb-3">Top estacionamientos</h2>
                    </div>
                    <!-- Card content -->
                    <div class="card-body card-body-cascade text-center">
                        <canvas id="horizontalBar"></canvas>
                    </div>
                    <!-- Card content -->
                </div>
                <!-- Card -->
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script>
        //pie
        var ctxP = document.getElementById("pieChart").getContext('2d');
        var myPieChart = new Chart(ctxP, {
            type: 'pie',
            data: {
                labels: ["Tickets pendientes", "Rechazados", "Facturados"],
                datasets: [{
                    data: [ {{$TotalGraf}}, {{$rechazo}} , {{$fact}}],
                    backgroundColor: ["#ffbb33", "#ff4444", "#00c851", "#949FB1", "#4D5360"],
                    hoverBackgroundColor: ["#ffbb33", "#ff4444", "#00c851", "#A8B3C5", "#616774"]
                }]
            },
            options: {
                responsive: true
            }
        });
    </script>
    <script>
        new Chart(document.getElementById("horizontalBar"), {
            "type": "horizontalBar",
            "data": {
                "labels": [
                    @php
                    if($est->count()){
                        foreach($est as $item){
                            echo '"'.$item->nombre.'",';
                        }
                    }
                    @endphp
                ],
                "datasets": [{
                    "label": "Facturas",
                    "data": [
                        @php
                            if($est->count()){
                                foreach($est as $item){
                                    echo $item->fact.',';
                                }
                            }
                        @endphp
                    ],
                    "fill": false,
                    "backgroundColor": ["rgba(255, 99, 132, 0.2)", "rgba(255, 159, 64, 0.2)",
                        "rgba(255, 205, 86, 0.2)", "rgba(75, 192, 192, 0.2)", "rgba(54, 162, 235, 0.2)",
                        "rgba(153, 102, 255, 0.2)", "rgba(201, 203, 207, 0.2)"
                    ],
                    "borderColor": ["rgb(255, 99, 132)", "rgb(255, 159, 64)", "rgb(255, 205, 86)",
                        "rgb(75, 192, 192)", "rgb(54, 162, 235)", "rgb(153, 102, 255)", "rgb(201, 203, 207)"
                    ],
                    "borderWidth": 1
                }]
            },
            "options": {
                "scales": {
                    "xAxes": [{
                        "ticks": {
                            "beginAtZero": true
                        }
                    }]
                }
            }
        });

    </script>
    <script type="text/javascript">
        function actualizar() {
            location.reload(true);
        }

        //Función para actualizar cada 4 segundos(4000 milisegundos)
        setInterval("actualizar()", 360000);
    </script>
@endpush
