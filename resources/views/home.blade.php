@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <!-- Jumbotron -->
        <!-- Card -->
        <div class="card testimonial-card ">

            <!-- Background color -->
            <div class="card-up central"></div>

            <!-- Avatar -->
            <div class="avatar mx-auto white">
                <img src="img/Logo.jpg" class="rounded-circle" alt="OCE">
            </div>

            <!-- Content -->
            <div class="card-body">
                <!-- Name -->
                <h4 class="card-title">CENTRAL OPERADORA DE ESTACIONAMIENTOS</h4>
                <hr>
                <!-- Quotation -->
                <p style="color:blue;">¡Bienvenido {{ Auth::user()->name }}!</p>
            </div>

        </div>
        <!-- Card -->
        <!-- Jumbotron -->
    </div>
</div>
@endsection
