@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <!-- Card -->
            <div class="card card-cascade">
                <!-- Card image -->
                <div class="view view-cascade gradient-card-header central">
                    <!-- Title -->
                    <h2 class="card-header-title mb-3">Validación sin estacionameinto</h2>
                </div>
                <!-- Card content -->
                <div class="card-body card-body-cascade text-center">
                    <!--Blue select-->
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">RFC Cliente</th>
                                <th scope="col">Total</th>
                                <th scope="col">Fecha de entrada</th>
                                <th scope="col">Estacionamiento</th>
                                <th scope="col">Estatus</th>
                                <th scope="col">Datos</th>
                                <th scope="col">Foto</th>
                                <th scope="col">Modificar</th>
                                <th scope="col">Validar</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($tickets as $ticket)
                                <tr>
                                    <th scope="row">{{$ticket->factura }}</th>
                                    <td>{{$ticket->RFC}}</td>
                                    <td>{{$ticket->total_ticket}}</td>
                                    <td>{{$ticket->fecha_emision}}</td>
                                    <td>({{$ticket->no_est}}) {{$ticket->nombre}}</td>
                                    @switch($ticket->estatus)
                                        @case('Rechazo')
                                        <td><strong class="red-text material-tooltip-email" data-toggle="tooltip"
                                                    data-placement="bottom" title="Rechazada"><i
                                                    class="fas fa-minus-circle" style="font-size: 30px;"></i></strong>
                                        </td>
                                        @break
                                        @case('validar')
                                        <td><strong class="blue-text material-tooltip-email" data-toggle="tooltip"
                                                    data-placement="bottom" title="Validar"><i
                                                    class="fas fa-exclamation-circle"
                                                    style="font-size: 30px;"></i></strong></td>
                                        @break
                                        @case('valido')
                                        <td><strong class="green-text material-tooltip-email" data-toggle="tooltip"
                                                    data-placement="bottom" title="Valida"><i
                                                    class="fas fa-check-circle"
                                                    style="font-size: 30px;"></i></strong>
                                        </td>
                                        @break
                                        @case('sin_fact')
                                        <td><strong class="blue-text material-tooltip-email" data-toggle="tooltip"
                                                    data-placement="bottom" title="Sin estacionamiento"><i
                                                    class="fas fa-parking"
                                                    style="font-size: 30px;"></i></strong></td>
                                        @break
                                    @endswitch
                                    <td>
                                        <a class="btn-floating btn-sm central"
                                           data-target="#DatosModal_{{$ticket->id}}" herf="" data-toggle="modal">
                                            <i class="far fa-address-card"></i>
                                        </a>
                                    </td>
                                    @if($ticket->id_tipo == 1)
                                        <td><a class="btn-floating btn-sm central"
                                               data-target="#ImgModal{{$ticket->id}}" herf="" data-toggle="modal"
                                            ><i class="far fa-image"></i></a></td>
                                    @else
                                        <td>{{__('Sin imagen')}}</td>
                                    @endif
                                    <td>
                                        <a class="btn-floating btn-sm central" data-target="#Modificar_{{$ticket->id}}"
                                           herf="" data-toggle="modal"><i class="far fa-edit"></i></a>
                                    </td>
                                    <td>
                                        <a class="btn-floating btn-sm light-green" href="{{route('ticket.valido2',$ticket->id)}}"><i
                                                class="far fa-check-circle"></i></a>
                                        <a class="btn-floating btn-sm red" data-target="#Rechazo_{{$ticket->id}}"
                                           herf="" data-toggle="modal"
                                        ><i class="far fa-times-circle"></i></a>
                                    </td>
                                </tr>
                                <!-- Modal -->
                                <div class="modal fade" id="ImgModal{{$ticket->id}}" tabindex="-1" role="dialog"
                                     aria-labelledby="exampleModalLabel"
                                     aria-hidden="true" style="overflow-y: scroll;">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Imagen</h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <img src="data:image/jpg;base64,{{$ticket->imagen}}"
                                                     class="img-fluid" alt="Responsive image">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal fade" id="DatosModal_{{$ticket->id}}" tabindex="-1" role="dialog"
                                     aria-labelledby="exampleModalLabel"
                                     aria-hidden="true" style="overflow-y: scroll;">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Datos</h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <form>
                                                    <div class="col-md-12 mb-12">
                                                        <div class="md-form">
                                                            <input type="text" id="rfc" class="form-control"
                                                                   value="{{$ticket->RFC}}">
                                                            <label for="rfc">RFC</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 mb-12">
                                                        <div class="md-form">
                                                            <input type="text" id="inputIconEx2" class="form-control"
                                                                   value="{{$ticket->Razon_social}}">
                                                            <label for="inputIconEx2">Razon Social</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 mb-12">
                                                        <div class="md-form">
                                                            <input type="text" id="inputIconEx1" class="form-control"
                                                                   value="{{$ticket->email}}">
                                                            <label for="inputIconEx1">E-mail</label>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal fade" id="Rechazo_{{$ticket->id}}" tabindex="-1" role="dialog"
                                     aria-labelledby="exampleModalLabel"
                                     aria-hidden="true" style="overflow-y: scroll;">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Rechazo</h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <form method="POST" action="{{route('ticket.rechazo2')}}">
                                                    @csrf
                                                    <input type="hidden" name="id" value="{{$ticket->id}}">
                                                    <div class="form-group">
                                                        <label for="exampleFormControlTextarea3">¿Por que se
                                                            Rechaza?</label>
                                                        <textarea class="form-control" id="exampleFormControlTextarea3"
                                                                  name="motivo" rows="7" required></textarea>
                                                    </div>
                                                    <button type="submit"
                                                            class="btn btn-outline-danger btn-rounded waves-effect">
                                                        Rechazar
                                                    </button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal fade" id="Modificar_{{$ticket->id}}" tabindex="-1" role="dialog"
                                     aria-labelledby="exampleModalLabel"
                                     aria-hidden="true" style="overflow-y: scroll;">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Datos</h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <form method="POST" action="{{route('actualiza2')}}">
                                                    @csrf
                                                    <input name="ticket" type="hidden" value="{{$ticket->id}}">
                                                    <div class="col-md-12 mb-12">
                                                        <div class="md-form">
                                                            <input type="number" id="no_est" name="no_est" class="form-control"
                                                                   value="{{$ticket->no_est}}">
                                                            <label for="no_est">Estacionamiento</label>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer d-flex justify-content-center">
                                                        <button class="btn btn-default">Actualizar</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @empty
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                    <!--/Blue select-->
                </div>
                <!-- Card content -->

            </div>
            <!-- Card -->
        </div>
    </div>
@endsection
