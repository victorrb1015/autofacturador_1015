@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <!-- Card -->
            <div class="card card-cascade col-md-12">
                <!-- Card image -->
                <div class="view view-cascade gradient-card-header central">
                    <!-- Title -->
                    <h2 class="card-header-title mb-3">Tickets rechazados</h2>
                </div>
                <!-- Card content -->
                <div class="card-body card-body-cascade text-center">
                    <!--Blue select-->
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">RFC Cliente</th>
                                <th scope="col">Razón Social</th>
                                <th scope="col">Total</th>
                                <th scope="col">Fecha</th>
                                <th scope="col">Estatus</th>
                                <th scope="col">Foto</th>
                                <th scope="col">Tipo</th>
                                <th scope="col">Motivo</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($tickets as $ticket)
                                <tr>
                                    <th scope="row">{{$ticket->factura }}</th>
                                    <td>{{$ticket->RFC}}</td>
                                    <td>{{$ticket->Razon_social}}</td>
                                    <td>{{$ticket->total_ticket}}</td>
                                    <td>{{$ticket->fecha_emision}}</td>
                                    @switch($ticket->estatus)
                                        @case('Rechazo')
                                        <td><strong class="red-text material-tooltip-email" data-toggle="tooltip"
                                                    data-placement="bottom" title="Rechazada"><i
                                                    class="fas fa-minus-circle" style="font-size: 30px;"></i></strong>
                                        </td>
                                        @break
                                        @case('validar')
                                        <td><strong class="blue-text material-tooltip-email" data-toggle="tooltip"
                                                    data-placement="bottom" title="Validar"><i
                                                    class="fas fa-exclamation-circle"
                                                    style="font-size: 30px;"></i></strong></td>
                                        @break
                                        @case('valido')
                                        <td><strong class="green-text material-tooltip-email" data-toggle="tooltip"
                                                    data-placement="bottom" title="Valida"><i
                                                    class="fas fa-check-circle"
                                                    style="font-size: 30px;"></i></i></strong>
                                        </td>
                                        @break
                                        @case('sin_fact')
                                        <td><strong class="blue-text material-tooltip-email" data-toggle="tooltip"
                                                    data-placement="bottom" title="Sin estacionamiento"><i
                                                    class="fas fa-parking"
                                                    style="font-size: 30px;"></i></strong></td>
                                        @break
                                    @endswitch
                                    @if($ticket->id_tipo === 1)
                                        <td><a class="btn-floating btn-sm central"
                                               data-target="#ImgModal{{$ticket->id}}" herf="" data-toggle="modal"
                                            ><i class="far fa-image"></i></a></td>
                                    @else
                                        <td>{{__('Sin imagen')}}</td>
                                    @endif
                                    @if($ticket->id_tipo == 1)
                                        <td>{{__('Manual')}}</td>
                                    @else
                                        <td>{{__('Automático')}}</td>
                                    @endif
                                    <td>
                                        <a class="btn-floating btn-sm red" data-target="#Rechazo_{{$ticket->id}}" herf="" data-toggle="modal"
                                        ><i class="fas fa-book"></i></a>
                                    </td>
                                </tr>
                                <!-- Modal -->
                                <div class="modal fade" id="ImgModal{{$ticket->id}}" tabindex="-1" role="dialog"
                                     aria-labelledby="exampleModalLabel"
                                     aria-hidden="true" style="overflow-y: scroll;">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Imagen</h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <img src="data:image/jpg;base64,{{$ticket->imagen}}" class="img-fluid"
                                                     alt="Responsive image">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal fade" id="Rechazo_{{$ticket->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                     aria-hidden="true" style="overflow-y: scroll;">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Rechazo</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <form>
                                                    @csrf
                                                    <input type="hidden" name="id" value="{{$ticket->id}}">
                                                    <div class="form-group">
                                                        <label>¿Por que se Rechaza?</label>
                                                        <br>
                                                        <label>{{$ticket->coment}}</label>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @empty
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                    <!--/Blue select-->
                </div>
                <!-- Card content -->

            </div>
            <!-- Card -->
        </div>
    </div>
@endsection
