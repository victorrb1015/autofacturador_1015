@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center ">
            <!-- Card -->
            <div class="card card-cascade col-md-12">

                <!-- Card image -->
                <div class="view view-cascade gradient-card-header central">

                    <!-- Title -->
                    <h2 class="card-header-title mb-3">Usuarios</h2>

                </div>

                <!-- Card content -->
                <div class="card-body card-body-cascade text-center">
                    <div class="table-responsive">
                        <table class="table table-striped table-sm">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Nombre</th>
                                <th>Correo</th>
                                <th>Bloqueo</th>
                                <th>Editar Contraseña</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($user as $item)
                                <tr>
                                    <td>{{$item->id}}</td>
                                    <td>{{$item->name}}</td>
                                    <td>{{$item->email}}</td>
                                    @if($item->deleted_at == null)
                                        <td><a class="btn-floating btn-sm btn-danger"
                                               href="{{route('baja',$item->id)}}"><i class="fas fa-user-minus"></i></a>
                                        </td>
                                    @else
                                        <td><a class="btn-floating btn-sm btn-success"
                                               href="{{route('cura',$item->id)}}"><i class="fas fa-user-plus"></i></a>
                                        </td>
                                    @endif
                                    <td><a class="btn-floating btn-sm btn-light-green"
                                           data-target="#Contraseña_{{$item->id}}"
                                           data-id="#pass_{{$item->id}}"
                                           href="" data-toggle="modal"><i class="fas fa-key"></i></a>
                                    </td>
                                    <div class="modal fade" id="Contraseña_{{$item->id}}" tabindex="-1" role="dialog"
                                         aria-labelledby="exampleModalLabel"
                                         aria-hidden="true" style="overflow-y: scroll;">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Cambiar
                                                        Contraseña</h5>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form method="POST" action="{{route('update')}}">
                                                        @csrf
                                                        <input hidden value="{{$item->id}}" name="user" type="number">
                                                        <div class="md-form input-group mb-3">
                                                            <div class="input-group-prepend">
                                                                <button class="btn btn-md btn-default m-0 px-3" type="button"
                                                                        onclick="funcion('#pass_{{$item->id}}')"
                                                                >Generar <i class="fas fa-cog"></i></button>
                                                            </div>
                                                            <input type="text" class="form-control" placeholder=""  id="pass_{{$item->id}}" name="pass">
                                                        </div>
                                                        <button type="submit"
                                                                class="btn btn-outline-success btn-rounded waves-effect btn-block">
                                                            Success
                                                        </button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </tr>
                            @empty
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- Card content -->

            </div>
            <!-- Card -->
        </div>
    </div>
@endsection

@push('script')
    <script>
        function funcion (a) {
            long = parseInt(13);
            var caracteres = "abcdefghijkmnpqrtuvwxyzABCDEFGHIJKLMNPQRTUVWXYZ012346789#%&!¡$*";
            var contraseña = "";
            for (i = 0; i < long; i++) contraseña += caracteres.charAt(Math.floor(Math.random() * caracteres.length));
            $(a).val(contraseña);
        }
    </script>
@endpush

