@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <!-- Jumbotron -->
            <!-- Card -->
            <div class="card card-cascade wider col-lg-12">

                <!-- Card image -->
                <div class="view view-cascade gradient-card-header central">

                    <!-- Title -->
                    <h2 class="card-header-title mb-3">{{__('Nuevo Estacionamiento')}}</h2>

                </div>

                <!-- Card content -->
                <div class="card-body card-body-cascade text-center">
                    <form method="POST" action="{{route('park.nuevo')}}">
                        @csrf
                        <div class="row text-left">
                            <!-- Grid column -->
                            <div class="col-md-3 mb-3">
                                <!-- Email validation -->
                                <div class="md-form">
                                    <i class="fas fa-parking prefix"></i>
                                    <input type="number" id="park" name="park" class="form-control validate"
                                           required>
                                    <label for="park" data-error="incorrecto" data-success="Correcto">Número</label>
                                </div>
                            </div>
                            <!-- Grid column -->
                            <!-- Grid column -->
                            <div class="col-md-9 mb-9">
                                <!-- Password validation -->
                                <div class="md-form">
                                    <i class="fas fa-parking prefix"></i>
                                    <input type="text" id="form7" name="N_est" class="form-control validate"
                                           required>
                                    <label for="form7" data-error="incorrecto" data-success="Correcto">Nombre del
                                        estacionamiento</label>
                                </div>
                            </div>
                        </div>
                        <div class="row text-left">
                            <div class="col-md-12">
                                <!-- Password validation -->
                                <div class="md-form">
                                    <select class="browser-default custom-select" name="org">
                                        <option disabled selected>Razón Social</option>
                                        @forelse($org as $itemOrg)
                                            <option value="{{$itemOrg->id}}">{{$itemOrg->Razon_social}}</option>
                                            @empty
                                            <option value="">No hay valores</option>
                                        @endforelse
                                    </select>
                                </div>
                            </div>
                        </div>
                        <h2>Dirección</h2>
                        <div class="row text-left">
                            <div class="col-md-6 mb-6">
                                <div class="md-form">
                                    <i class="fas fa-parking prefix"></i>
                                    <input type="text" id="calle" name="calle" class="form-control validate"
                                           required>
                                    <label for="calle" data-error="incorrecto" data-success="Correcto">Calle</label>
                                </div>
                            </div>
                            <div class="col-md-2 mb-2">
                                <div class="md-form">
                                    <i class="fas fa-parking prefix"></i>
                                    <input type="text" id="no_ext" name="no_ext" class="form-control validate"
                                           required>
                                    <label for="no_ext" data-error="incorrecto" data-success="Correcto">No
                                        Exterior</label>
                                </div>
                            </div>
                            <div class="col-md-2 mb-2">
                                <div class="md-form">
                                    <i class="fas fa-parking prefix"></i>
                                    <input type="text" id="no_int" name="no_int" class="form-control validate"
                                           >
                                    <label for="no_int" data-error="incorrecto" data-success="Correcto">No
                                        Interior</label>
                                </div>
                            </div>
                            <div class="col-md-2 mb-2">
                                <div class="md-form">
                                    <i class="fas fa-parking prefix"></i>
                                    <input type="text" id="cp" name="cp" class="form-control validate"
                                           required>
                                    <label for="cp" data-error="incorrecto" data-success="Correcto">CP</label>
                                </div>
                            </div>
                            <div class="col-md-3 mb-3">
                                <div class="md-form">
                                    <i class="fas fa-parking prefix"></i>
                                    <input type="text" id="colonia" name="colonia" class="form-control validate"
                                           required>
                                    <label for="colonia" data-error="incorrecto" data-success="Correcto">Colonia</label>
                                </div>
                            </div>
                            <div class="col-md-3 mb-3">
                                <div class="md-form">
                                    <i class="fas fa-parking prefix"></i>
                                    <input type="text" id="municipio" name="municipio" class="form-control validate"
                                           required>
                                    <label for="municipio" data-error="incorrecto"
                                           data-success="Correcto">Municipio</label>
                                </div>
                            </div>
                            <div class="col-md-3 mb-3">
                                <div class="md-form">
                                    <i class="fas fa-parking prefix"></i>
                                    <input type="text" id="estado" name="estado" class="form-control validate"
                                           required>
                                    <label for="estado" data-error="incorrecto" data-success="Correcto">Estado</label>
                                </div>
                            </div>
                            <div class="col-md-3 mb-3">
                                <div class="md-form">
                                    <i class="fas fa-parking prefix"></i>
                                    <input type="text" id="dist_regio" name="dist_regio" class="form-control validate"
                                           required>
                                    <label for="dist_regio" data-error="incorrecto" data-success="Correcto">Distrito/Región</label>
                                </div>
                            </div>
                        </div>
                        <h2>Caracteristicas</h2>
                        <div class="row text-left">
                            <div class="col-md-6 mb-6">
                                <div class="md-form">
                                    <div class="switch red">
                                        <label>
                                            Manual
                                            <input type="checkbox" name="tipo">
                                            <span class="lever"></span> Automático
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 mb-6">
                                <div class="md-form">
                                    <div class="switch red">
                                        <label>
                                            No Facturable
                                            <input type="checkbox" name="fact">
                                            <span class="lever"></span> Facturable
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <br>
                        <div class="row text-left">
                            <div class="col-md-6 mb-6">
                                <div class="md-form">
                                    <i class="fas fa-parking prefix"></i>
                                    <input type="text" id="marca" name="marca" class="form-control validate"
                                           required>
                                    <label for="marca" data-error="incorrecto"
                                           data-success="Correcto">Marca</label>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-outline-success btn-rounded waves-effect">Success</button>
                    </form>
                </div>
                <!-- Card content -->

            </div>
            <!-- Card -->
            <!-- Jumbotron -->
        </div>
    </div>
@endsection
