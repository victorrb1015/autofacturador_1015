@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <!-- Jumbotron -->
            <!-- Card -->
            <div class="card card-cascade wider col-lg-12">

                <!-- Card image -->
                <div class="view view-cascade gradient-card-header central">

                    <!-- Title -->
                    <h2 class="card-header-title mb-3">{{$no_est->nombre}}</h2>

                </div>

                <!-- Card content -->
                <div class="card-body card-body-cascade text-center">
                    <form method="POST" action="">
                        <div class="row text-left">
                            <!-- Grid column -->
                            <div class="col-md-3 mb-3">
                                <!-- Email validation -->
                                <div class="md-form">
                                    <i class="fas fa-parking prefix"></i>
                                    <input type="number" id="park" name="park" class="form-control validate"
                                           value="{{$no_est->no_est}}">
                                    <label for="park" data-error="incorrecto" data-success="Correcto">Numero</label>
                                </div>
                            </div>
                            <!-- Grid column -->
                            <!-- Grid column -->
                            <div class="col-md-5 mb-5">
                                <!-- Password validation -->
                                <div class="md-form">
                                    <i class="fas fa-parking prefix"></i>
                                    <input type="text" id="form7" class="form-control validate"
                                           value="{{$no_est->nombre}}">
                                    <label for="form7" data-error="incorrecto" data-success="Correcto">Nombre del
                                        estacionamiento</label>
                                </div>
                            </div>
                            <div class="col-md-2 mb-2">
                                <!-- Password validation -->
                                <div class="md-form">
                                    <i class="fas fa-parking prefix"></i>
                                    <input type="text" id="serie" class="form-control validate"
                                           value="{{$no_est->serie}}">
                                    <label for="serie" data-error="incorrecto" data-success="Correcto">Serie</label>
                                </div>
                            </div>
                            <div class="col-md-2 mb-2">
                                <!-- Password validation -->
                                <div class="md-form">
                                    <i class="fas fa-parking prefix"></i>
                                    <input type="number" id="folio" class="form-control validate"
                                           value="{{$no_est->folio}}">
                                    <label for="folio" data-error="incorrecto" data-success="Correcto">folio</label>
                                </div>
                            </div>
                            <!-- Grid column -->
                        </div>
                        <div class="row text-left">
                            <div class="col-md-7">
                                <!-- Password validation -->
                                <div class="md-form">
                                    <i class="fas fa-book-open prefix"></i>
                                    <input type="text" id="RFC" name="RFC" class="form-control validate"
                                           value="{{$org->Razon_social}}">
                                    <label for="RFC" data-error="incorrecto" data-success="Correcto">Razon social
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <!-- Password validation -->
                                <div class="md-form">
                                    <i class="fas fa-book-open prefix"></i>
                                    <input type="text" id="RS" name="RS" class="form-control validate"
                                           value="{{$org->RFC}}">
                                    <label for="RS" data-error="incorrecto" data-success="Correcto">RFC
                                    </label>
                                </div>
                            </div>
                        </div>
                        <h2>Dirección</h2>
                        <div class="row text-left">
                            <div class="col-md-6 mb-6">
                                <div class="md-form">
                                    <i class="fas fa-parking prefix"></i>
                                    <input type="text" id="calle" name="calle" class="form-control validate"
                                           value="{{$no_est->calle}}">
                                    <label for="calle" data-error="incorrecto" data-success="Correcto">Calle</label>
                                </div>
                            </div>
                            <div class="col-md-2 mb-2">
                                <div class="md-form">
                                    <i class="fas fa-parking prefix"></i>
                                    <input type="text" id="no_ext" name="no_ext" class="form-control validate"
                                           value="{{$no_est->no_ext}}">
                                    <label for="no_ext" data-error="incorrecto" data-success="Correcto">No
                                        Exterior</label>
                                </div>
                            </div>
                            <div class="col-md-2 mb-2">
                                <div class="md-form">
                                    <i class="fas fa-parking prefix"></i>
                                    <input type="text" id="no_int" name="no_int" class="form-control validate"
                                           value="{{$no_est->no_int}}">
                                    <label for="no_int" data-error="incorrecto" data-success="Correcto">No
                                        Interior</label>
                                </div>
                            </div>
                            <div class="col-md-2 mb-2">
                                <div class="md-form">
                                    <i class="fas fa-parking prefix"></i>
                                    <input type="text" id="cp" name="cp" class="form-control validate"
                                           value="{{$no_est->cp}}">
                                    <label for="cp" data-error="incorrecto" data-success="Correcto">CP</label>
                                </div>
                            </div>
                            <div class="col-md-4 mb-4">
                                <div class="md-form">
                                    <i class="fas fa-parking prefix"></i>
                                    <input type="text" id="colonia" name="colonia" class="form-control validate"
                                           value="{{$no_est->colonia}}">
                                    <label for="colonia" data-error="incorrecto" data-success="Correcto">Colonia</label>
                                </div>
                            </div>
                            <div class="col-md-4 mb-4">
                                <div class="md-form">
                                    <i class="fas fa-parking prefix"></i>
                                    <input type="text" id="municipio" name="municipio" class="form-control validate"
                                           value="{{$no_est->municipio}}">
                                    <label for="municipio" data-error="incorrecto"
                                           data-success="Correcto">Municipio</label>
                                </div>
                            </div>
                            <div class="col-md-4 mb-4">
                                <div class="md-form">
                                    <i class="fas fa-parking prefix"></i>
                                    <input type="text" id="estado" name="estado" class="form-control validate"
                                           value="{{$no_est->estado}}">
                                    <label for="estado" data-error="incorrecto" data-success="Correcto">Estado</label>
                                </div>
                            </div>
                        </div>
                        <h2>Caracteristicas</h2>
                        <div class="row text-left">
                            <div class="col-md-6 mb-6">
                                <div class="md-form">
                                    <div class="switch red">
                                        <label>
                                            Manual
                                            <input type="checkbox"
                                                   @if($no_est->Automatico)
                                                   checked
                                                @endif
                                            >
                                            <span class="lever"></span> Automático
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 mb-6">
                                <div class="md-form">
                                    <div class="switch red">
                                        <label>
                                            No Facturable
                                            <input type="checkbox"
                                                   @if($no_est->Facturable)
                                                   checked
                                                @endif
                                            >
                                            <span class="lever"></span> Facturable
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <br>
                        <div class="row text-left">
                            <div class="col-md-6 mb-6">
                                <div class="md-form">
                                    <i class="fas fa-parking prefix"></i>
                                    <input type="text" id="marca" name="marca" class="form-control validate"
                                           value="{{$no_est->marca}}">
                                    <label for="marca" data-error="incorrecto"
                                           data-success="Correcto">Marca</label>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- Card content -->

            </div>
            <!-- Card -->
            <!-- Jumbotron -->
        </div>
    </div>
@endsection
