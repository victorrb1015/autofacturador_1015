@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="row">
                    <!-- Card -->
                    <div class="card card-cascade narrower col-md-12">
                        <!-- Card image -->
                        <div class="view view-cascade gradient-card-header central">
                            <!-- Title -->
                            <h2 class="card-header-title">Top 5 clientes</h2>
                        </div>
                        <!-- Card content -->
                        <div class="card-body card-body-cascade text-center">
                            <ul class="list-group">
                                @forelse ($rfc as $itemRFC)
                                    <li class="list-group-item d-flex justify-content-between align-items-center">
                                        {{$itemRFC->RFC}}
                                        <span class="badge badge-primary badge-pill">{{$itemRFC->fact}}</span>
                                    </li>
                                @empty
                                @endforelse
                            </ul>
                        </div>
                    </div>
                    <!-- Card -->
                </div>
                <div class="row py-4">
                    <!-- Card -->
                    <div class="card card-cascade narrower col-md-12">
                        <!-- Card image -->
                        <div class="view view-cascade gradient-card-header central">
                            <!-- Title -->
                            <h2 class="card-header-title">Facturado</h2>
                        </div>
                        <!-- Card content -->
                        <div class="card-body card-body-cascade">
                            <canvas id="pieChart"></canvas>
                            <br>
                            <p class="h5 text-center">Datos</p>
                            <ul class="list-group">
                                <li class="list-group-item">
                                    <div class="md-v-line"></div>
                                    <i class="fas fa-ticket-alt mr-4 pr-3"></i> Tickets del día
                                    <span class="badge badge-primary badge-pill">{{$total}}</span>
                                </li>
                                <li class="list-group-item">
                                    <div class="md-v-line"></div>
                                    <i class="fas fa-user mr-5"></i>Facturas del día
                                    <span class="badge badge-primary badge-pill">{{$fact}}</span>
                                </li>
                                <li class="list-group-item">
                                    <div class="md-v-line"></div>
                                    <i class="fas fa-dollar-sign mr-5"></i>Facturado
                                    <span class="badge badge-success badge-pill">{{$facturado}}</span>
                                </li>
                                <li class="list-group-item">
                                    <div class="md-v-line"></div>
                                    <i class="fas fa-exclamation-circle mr-5"></i>Rechazadas
                                    <span class="badge badge-danger badge-pill">{{$rechazo}}</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- Card -->
                </div>
            </div>
            <div class="col-md-8">
                <!-- Card -->
                <div class="card card-cascade">
                    <!-- Card image -->
                    <div class="view view-cascade gradient-card-header central">
                        <!-- Title -->
                        <h2 class="card-header-title mb-3">Tickets</h2>
                    </div>
                    <!-- Card content -->
                    <div class="card-body card-body-cascade text-center">
                        <!--Blue select-->
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">RFC Cliente</th>
                                    <th scope="col">Razón Social</th>
                                    <th scope="col">Total</th>
                                    <th scope="col">Fecha</th>
                                    <th scope="col">Estatus</th>
                                    <th scope="col">Foto</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($tickets as $ticket)
                                    <tr>
                                        <th scope="row">{{$ticket->factura }}</th>
                                        <td>{{$ticket->RFC}}</td>
                                        <td>{{$ticket->Razon_social}}</td>
                                        <td>{{$ticket->total_ticket}}</td>
                                        <td>{{$ticket->fecha_emision}}</td>
                                        @switch($ticket->estatus)
                                            @case('Rechazo')
                                            <td><strong class="red-text material-tooltip-email" data-toggle="tooltip"
                                                        data-placement="bottom" title="Rechazada"><i
                                                        class="fas fa-minus-circle"
                                                        style="font-size: 30px;"></i></strong>
                                            </td>
                                            @break
                                            @case('validar')
                                            <td><strong class="blue-text material-tooltip-email" data-toggle="tooltip"
                                                        data-placement="bottom" title="Validar"><i
                                                        class="fas fa-exclamation-circle"
                                                        style="font-size: 30px;"></i></strong></td>
                                            @break
                                            @case('valido')
                                            <td><strong class="green-text material-tooltip-email" data-toggle="tooltip"
                                                        data-placement="bottom" title="Valida"><i
                                                        class="fas fa-check-circle"
                                                        style="font-size: 30px;"></i></strong>
                                            </td>
                                            @break
                                            @case('sin_fact')
                                            <td><strong class="blue-text material-tooltip-email" data-toggle="tooltip"
                                                        data-placement="bottom" title="Sin estacionamiento"><i
                                                        class="fas fa-parking"
                                                        style="font-size: 30px;"></i></strong></td>
                                            @break
                                            @case('SAT')
                                            <td><strong class="yellow-text material-tooltip-email" data-toggle="tooltip"
                                                        data-placement="bottom" title="Factura del SAT"><i
                                                        class="fas fa-search-dollar"
                                                        style="font-size: 30px;"></i></strong></td>
                                            @break
                                        @endswitch
                                        @if($ticket->id_tipo == 1)
                                            <td><a class="btn-floating btn-sm central"
                                                   data-target="#ImgModal{{$ticket->id}}" herf="" data-toggle="modal"
                                                ><i class="far fa-image"></i></a></td>
                                        @else
                                            <td>{{__('Sin imagen')}}</td>
                                        @endif
                                    </tr>
                                    <!-- Modal -->
                                    <div class="modal fade" id="ImgModal{{$ticket->id}}" tabindex="-1" role="dialog"
                                         aria-labelledby="exampleModalLabel"
                                         aria-hidden="true" style="overflow-y: scroll;">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Imagen</h5>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <img src="data:image/jpg;base64,{{$ticket->imagen}}"
                                                         class="img-fluid"
                                                         alt="Responsive image">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @empty
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                        <!--/Blue select-->
                    </div>
                    <!-- Card content -->
                </div>
                <!-- Card -->
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script>
        $(function () {
            $('.material-tooltip-email').tooltip({
                template: '<div class="tooltip md-tooltip-email"><div class="tooltip-arrow md-arrow"></div><div class="tooltip-inner md-inner-email"></div></div>'
            });
        })
    </script>
    <script>
        //pie
        var ctxP = document.getElementById("pieChart").getContext('2d');
        var myPieChart = new Chart(ctxP, {
            type: 'pie',
            data: {
                labels: ["Tickets pendientes", "Rechazados", "Facturados"],
                datasets: [{
                    data: [ {{$TotalGraf}}, {{$rechazo}} , {{$fact}}],
                    backgroundColor: ["#ffbb33", "#ff4444", "#00c851", "#949FB1", "#4D5360"],
                    hoverBackgroundColor: ["#ffbb33", "#ff4444", "#00c851", "#A8B3C5", "#616774"]
                }]
            },
            options: {
                responsive: true
            }
        });
    </script>
@endpush
