@extends('layouts.app')

@section('content')
    <style>
        table.dataTable thead .sorting:after,
        table.dataTable thead .sorting:before,
        table.dataTable thead .sorting_asc:after,
        table.dataTable thead .sorting_asc:before,
        table.dataTable thead .sorting_asc_disabled:after,
        table.dataTable thead .sorting_asc_disabled:before,
        table.dataTable thead .sorting_desc:after,
        table.dataTable thead .sorting_desc:before,
        table.dataTable thead .sorting_desc_disabled:after,
        table.dataTable thead .sorting_desc_disabled:before {
            bottom: .5em;
        }
    </style>
    <div class="container">
        <div class="row justify-content-center">
            <!-- Card -->
            <div class="card card-cascade wider">

                <!-- Card image -->
                <div class="view view-cascade gradient-card-header central">

                    <!-- Title -->
                    <h2 class="card-header-title mb-3">Estacionamientos</h2>
                </div>

                <!-- Card content -->
                <div class="card-body card-body-cascade text-center">
                    <div class="table-responsive">
                        <table id="dtBasicExample" class="table table-striped table-sm">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Nombre</th>
                                <th scope="col">Distrito/Región</th>
                                <th scope="col">Código Postal</th>
                                <th scope="col">Facturable</th>
                                <th scope="col">Modelo</th>
                                <th scope="col">Automático</th>
                                <th scope="col">Facturas</th>
                                <th scope="col">Información</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($est as $estItem)
                                <tr>
                                    <th scope="row">{{$estItem->no_est}}</th>
                                    <td>{{$estItem->nombre}}</td>
                                    <td>{{$estItem->dist_regio}}</td>
                                    <td>{{$estItem->cp}}</td>
                                    @if($estItem->Facturable)
                                        <td>{{__('Si')}}</td>
                                    @else
                                        <td>{{__('No Facturable')}}</td>
                                    @endif
                                    <td>{{$estItem->marca}}</td>
                                    @if($estItem->Automatico)
                                        <td>{{__('Si')}}</td>
                                    @else
                                        <td>{{__('No')}}</td>
                                    @endif
                                    <td><a class="btn-floating btn-sm central" href="{{route('park.fact', $estItem->no_est)}}"><i class="far fa-clipboard"></i></a></td>
                                    <td><a class="btn-floating btn-sm central" href="{{route('numero', $estItem->no_est)}}"><i class="fas fa-info-circle"></i></a></td>
                                </tr>
                            @empty
                                <tr>
                                    <th scope="row"></th>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                    {{$est->links()}}
                </div>
                <!-- Card content -->

            </div>
            <!-- Card -->
        </div>
    </div>
@endsection
@push('script')
<script>
    $(document).ready(function () {
        $('#dtBasicExample').DataTable();
        $('.dataTables_length').addClass('bs-select');
    });
</script>
@endpush
