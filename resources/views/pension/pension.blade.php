@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <!-- Card -->
            <div class="card card-cascade col-md-12">
                <!-- Card image -->
                <div class="view view-cascade gradient-card-header central">
                    <!-- Title -->
                    <h2 class="card-header-title mb-3">Pensiones</h2>
                </div>
                <!-- Card content -->
                <div class="card-body card-body-cascade text-center">
                    <!--Blue select-->
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Estacionameinto</th>
                                <th scope="col">Usuario</th>
                                <th scope="col">Correo</th>
                                <th scope="col">Tipo de pension</th>
                                <th scope="col">Tipo de pago</th>
                                <th scope="col">Forma de pago</th>
                                <th scope="col">Mes del pago</th>
                                <th scope="col">Fecha limite de pago</th>
                                <th scope="col">Require factura</th>
                                <th scope="col">Costos</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($p as $pen)
                                <tr>
                                    <td>{{$pen->num_pen}}</td>
                                    <td>{{$pen->no_est}}</td>
                                    <td>{{$pen->name}}</td>
                                    <td>{{$pen->email}}</td>
                                    <td>{{$pen->tipo_pen}}</td>
                                    <td>{{$pen->tipo_pago}}</td>
                                    <td>{{$pen->forma}}</td>
                                    @switch($pen->mes_pago)
                                        @case(1)
                                        <td>{{__('Enero')}}</td>
                                        @break
                                        @case(2)
                                        <td>{{__('Febrero')}}</td>
                                        @break
                                        @case(3)
                                        <td>{{__('Marzo')}}</td>
                                        @break
                                        @case(4)
                                        <td>{{__('Abril')}}</td>
                                        @break
                                        @case(5)
                                        <td>{{__('Mayo')}}</td>
                                        @break
                                        @case(6)
                                        <td>{{__('Junio')}}</td>
                                        @break
                                        @case(7)
                                        <td>{{__('Julio')}}</td>
                                        @break
                                        @case(8)
                                        <td>{{__('Agosto')}}</td>
                                        @break
                                        @case(9)
                                        <td>{{__('Septiembre')}}</td>
                                        @break
                                        @case(10)
                                        <td>{{__('Octubre')}}</td>
                                        @break
                                        @case(11)
                                        <td>{{__('Noviembre')}}</td>
                                        @break
                                        @case(12)
                                        <td>{{__('Diciembre')}}</td>
                                        @break
                                    @endswitch
                                    <td>{{$pen->fecha}}</td>
                                    @if($pen->factura)
                                        <td><strong class="green-text material-tooltip-email" data-toggle="tooltip"
                                                    data-placement="bottom" title="Si"><i
                                                    class="fas fa-check-circle"
                                                    style="font-size: 30px;"></i></strong>
                                        </td>
                                    @else
                                        <td><strong class="red-text material-tooltip-email" data-toggle="tooltip"
                                                    data-placement="bottom" title="No"><i
                                                    class="fas fa-times-circle" style="font-size: 30px;"></i></strong>
                                        </td>
                                    @endif
                                    <td>
                                        <a class="btn-floating btn-sm central" data-target="#Costo_{{$pen->id}}"
                                           herf="" data-toggle="modal">
                                            <i class="fas fa-dollar-sign"></i>
                                        </a>
                                    </td>
                                </tr>
                                <div class="modal fade" id="Costo_{{$pen->id}}" tabindex="-1" role="dialog"
                                     aria-labelledby="exampleModalLabel"
                                     aria-hidden="true" style="overflow-y: scroll;">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Costos</h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <div>
                                                    <form>
                                                        <div class="col-md-12 mb-12">
                                                            <div class="md-form">
                                                                <input type="text" id="Costo" class="form-control"
                                                                       value="{{$pen->costo_pension}}">
                                                                <label for="Costo">Costo</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb-12">
                                                            <div class="md-form">
                                                                <input type="text" id="Recargo" class="form-control"
                                                                       value="{{$pen->recargos}}">
                                                                <label for="Recargo">Recargo</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb-12">
                                                            <div class="md-form">
                                                                <input type="text" id="tarjeta" class="form-control"
                                                                       value="{{$pen->venta_tarjeta}}">
                                                                <label for="tarjeta">Venta de tarjeta</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb-12">
                                                            <div class="md-form">
                                                                <input type="text" id="de" class="form-control"
                                                                       value="{{$pen->repo_tarjeta}}">
                                                                <label for="de">Reposición de tarjeta</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 mb-12">
                                                            <div class="md-form">
                                                                <input type="text" id="pago" class="form-control"
                                                                       value="{{$pen->impor_pago}}">
                                                                <label for="pago">Importe de pago</label>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @empty
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                    <!--/Blue select-->
                </div>
                <!-- Card content -->

            </div>
            <!-- Card -->
        </div>
    </div>
@endsection
