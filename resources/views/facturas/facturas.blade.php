@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <!-- Card -->
            <div class="card card-cascade wider col-md-12">

                <!-- Card image -->
                <div class="view view-cascade gradient-card-header central">

                    <!-- Title -->
                    <h2 class="card-header-title mb-3">Facturas</h2>

                </div>

                <!-- Card content -->
                <div class="card-body card-body-cascade text-center">
                    <!--Table-->
                    <div class="table-responsive">
                        <table class="table table-striped">

                            <!--Table head-->
                            <thead>
                            <tr>
                                <th>Folio/Serie</th>
                                <th>UUID</th>
                                <th>Fecha</th>
                                <th>Cliente</th>
                                <th>Importe</th>
                                <th>IVA</th>
                                <th>Total</th>
                                <th>Estatus</th>
                                <th>Reenviar</th>
                            </tr>
                            </thead>
                            <!--Table head-->
                            <!--Table body-->
                            <tbody>
                            @forelse($fact as $factItem)
                                <tr>
                                    <td>{{$factItem->serie.$factItem->folio}}</td>
                                    <td>{{$factItem->uuid}}</td>
                                    <td>{{$factItem->fecha_emision}}</td>
                                    <td>{{$factItem->RFC}}</td>
                                    <td>{{$factItem->total_ticket}}</td>
                                    <td>{{$factItem->iva_factura}}</td>
                                    <td>{{$factItem->total_factura}}</td>
                                    @switch($factItem->estatus)
                                        @case('valido' || 'SAT')
                                        <td>Pagada</td>
                                        <td>
                                            <a class="btn-floating btn-sm btn-green" href="{{route('reenvio', $factItem)}}"><i class="fas fa-file-invoice-dollar"></i></a>
                                        </td>
                                    @endswitch

                                </tr>
                            @empty
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                    <!--Table-->
                </div>
                <!-- Card content -->
            </div>
            <!-- Card -->
        </div>
    </div>
@endsection
