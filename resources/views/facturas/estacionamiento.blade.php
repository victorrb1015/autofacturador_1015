@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <!-- Card -->
            <div class="card card-cascade wider">
                <!-- Card image -->
                <div class="view view-cascade gradient-card-header central">
                    <!-- Title -->
                    <h2 class="card-header-title mb-3">Estacionamiento</h2>
                </div>
                <!-- Card content -->
                <div class="card-body card-body-cascade text-center">
                        <!--Blue select-->
                        <div>
                            <form method="POST" action="{{ route('facturas.generar') }}">
                                @csrf
                                <div class="md-form input-group mb-3">
                                    <input type="number" id="no_est" name="no_est" class="form-control" min="1" pattern="^[0-9]+">
                                    <label for="no_est">Estacionamiento</label>
                                    <div class="input-group-prepend dropdown">
                                        <button type="submit" class="btn btn-md btn-elegant m-0 px-3">Buscar</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!--/Blue select-->
                </div>
                <!-- Card content -->

            </div>
            <!-- Card -->
        </div>
    </div>
@endsection
