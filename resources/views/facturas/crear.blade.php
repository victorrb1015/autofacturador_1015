@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <!-- Card -->
            <div class="card card-cascade wider col-lg-12">
                <!-- Card image -->
                <div class="view view-cascade gradient-card-header central">
                    <!-- Title -->
                    <h2 class="card-header-title mb-8">Nueva Factura</h2>
                </div>
                <!-- Card content -->
                <div class="card-body card-body-cascade">
                    <!--Blue select-->
                    <div>
                        <h2>Datos del estacionamiento</h2>
                        <form method="POST" action="{{ route('facturas.crear') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="row text-left">
                                <!-- Grid column -->
                                <div class="col-md-3 mb-3">
                                    <!-- Email validation -->
                                    <div class="md-form">
                                        <i class="fas fa-parking prefix"></i>
                                        <input type="number" id="park" name="park" class="form-control validate"
                                               value="{{$park->no_est}}">
                                        <label for="park" data-error="incorrecto" data-success="Correcto">Número</label>
                                    </div>
                                </div>
                                <!-- Grid column -->
                                <!-- Grid column -->
                                <div class="col-md-9 mb-9">
                                    <!-- Password validation -->
                                    <div class="md-form">
                                        <i class="fas fa-parking prefix"></i>
                                        <input type="text" id="form7" class="form-control validate"
                                               value="{{$park->nombre}}">
                                        <label for="form7" data-error="incorrecto" data-success="Correcto">Nombre del
                                            estacionamiento</label>
                                    </div>
                                </div>
                                <!-- Grid column -->
                            </div>
                            <div class="row text-left">
                                <div class="col-md-7">
                                    <!-- Password validation -->
                                    <div class="md-form">
                                        <i class="fas fa-book-open prefix"></i>
                                        <input type="text" id="RFC" name="RFC" class="form-control validate"
                                               value="{{$park->Razon_social}}">
                                        <label for="RFC" data-error="incorrecto" data-success="Correcto">Razón social
                                            Empresa</label>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <!-- Password validation -->
                                    <div class="md-form">
                                        <i class="fas fa-book-open prefix"></i>
                                        <input type="text" id="RS" name="RS" class="form-control validate"
                                               value="{{$park->RFC}}">
                                        <label for="RS" data-error="incorrecto" data-success="Correcto">RFC
                                            Empresa</label>
                                    </div>
                                </div>
                            </div>
                            <h2>Datos del cliente</h2>
                            <div class="row text-left">

                                <div class="col-md-7">
                                    <!-- Password validation -->
                                    <div class="md-form">
                                        <i class="fas fa-user prefix"></i>
                                        <input type="text" id="RS" name="RS" class="form-control validate"
                                               value="{{old('RS')}}" required>
                                        <label for="RS" data-error="incorrecto" data-success="Correcto">Razón social
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <!-- Password validation -->
                                    <div class="md-form">
                                        <i class="fas fa-user prefix"></i>
                                        <input type="text" id="RFC" name="RFC" class="form-control validate"
                                               value="{{old('RFC')}}" required>
                                        <label for="RFC" data-error="incorrecto" data-success="Correcto">RFC
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <!-- Password validation -->
                                    <div class="md-form">
                                        <i class="fas fa-at prefix"></i>
                                        <input type="email" id="email" name="email" class="form-control validate"
                                               value="{{old('email')}}" required>
                                        <label for="email" data-error="incorrecto" data-success="Correcto">Correo
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <select class="browser-default custom-select md-form" name="usoCFDI" required>
                                        <option value="" disabled selected>Elija un Uso CFDI*</option>
                                        <option value="G01">(G01) Adquisición de mercancias</option>
                                        <option value="G02">(G02) Devoluciones, descuentos o bonificaciones
                                        </option>
                                        <option value="G03">(G03) Gastos en general</option>
                                        <option value="I01">(I01) Construcciones</option>
                                        <option value="I02">(I02) Mobilario y equipo de oficina por
                                            inversiones
                                        </option>
                                        <option value="I03">(I03) Equipo de transporte</option>
                                        <option value="I04">(I04) Equipo de computo y accesorios</option>
                                        <option value="I05">(I05) Dados, troqueles, moldes, matrices y
                                            herramental
                                        </option>
                                        <option value="I06">(I06) Comunicaciones telefónicas</option>
                                        <option value="I07">(I07) Comunicaciones satelitales</option>
                                        <option value="I08">(I08) Otra maquinaria y equipo</option>
                                        <option value="D01">(D01) Honorarios médicos, dentales y gastos
                                            hospitalarios.
                                        </option>
                                        <option value="D02">(D02) Gastos médicos por incapacidad o
                                            discapacidad
                                        </option>
                                        <option value="D03">(D03) Gastos funerales.</option>
                                        <option value="D04">(D04) Donativos.</option>
                                        <option value="D05">(D05) Intereses reales efectivamente pagados por
                                            créditos hipotecarios (casa habitación).
                                        </option>
                                        <option value="D06">(D06) Aportaciones voluntarias al SAR.</option>
                                        <option value="D07">(D07) Primas por seguros de gastos médicos.
                                        </option>
                                        <option value="D08">(D08) Gastos de transportación escolar
                                            obligatoria.
                                        </option>
                                        <option value="D09">(D09) Depósitos en cuentas para el ahorro, primas
                                            que
                                            tengan como base planes de pensiones.
                                        </option>
                                        <option value="D10">(D10) Pagos por servicios educativos
                                            (colegiaturas)
                                        </option>
                                        <option value="P01">(P01) Por definir</option>
                                    </select>
                                </div>
                            </div>
                            <h2>Datos del pago</h2>
                            <div class="row text-left">
                                <div class="col-md-6">
                                    <select class="browser-default custom-select md-form" name="moneda" required>
                                        <option value="" disabled selected>Tipo de moneda</option>
                                        <option value="MXN">(MXN) Mexicana</option>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <select class="browser-default custom-select md-form" name="forma_pago" required>
                                        <option value="" disabled selected>Forma de pago</option>
                                        <option value="01">(01) Efectivo</option>
                                        <option value="02">(02) Cheque nominativo</option>
                                        <option value="03">(03) Transferencia electrónica de fondos</option>
                                        <option value="04">(04) Tarjeta de crédito</option>
                                        <option value="05">(05) Monedero electrónico</option>
                                        <option value="06">(06) Dinero electrónico</option>
                                        <option value="08">(08) Vales de despensa</option>
                                        <option value="28">(28) Tarjeta de débito</option>
                                        <option value="29">(29) Tarjeta de servicio</option>
                                        <option value="99">(99) Otros</option>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <select class="browser-default custom-select md-form" name="metodo_pago" required>
                                        <option value="" disabled selected>Método de Pago</option>
                                        <option value="PUE">Pago en una sola exhibición</option>
                                        <option value="PPD">Pago en parcialidades o diferido</option>
                                    </select>
                                </div>
                            </div>
                            <h2>Conceptos</h2>
                            <div class="row text-left">
                                <div class="col-md-12">
                                    <select class="browser-default custom-select md-form" name="concepto" required>
                                        <option value="" disabled selected>Conceptos</option>
                                        <option value="1">Tarifas del Parqueadero</option>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <!-- Password validation -->
                                    <div class="md-form">
                                        <i class="fas fa-list-ol prefix"></i>
                                        <input type="number" id="cantidad" name="cantidad" class="form-control validate"
                                               value="" required min="1" pattern="^[0-9]+">
                                        <label for="cantidad" data-error="incorrecto" data-success="Correcto">Cantidad
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <!-- Password validation -->
                                    <div class="md-form">
                                        <i class="fas fa-dollar-sign prefix"></i>
                                        <input type="text" id="precio" name="precio" class="form-control validate"
                                               value="" required>
                                        <label for="precio" data-error="incorrecto" data-success="Correcto">Precio
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="md-form">
                                        <textarea id="comment" name="comment" class="form-control md-textarea"
                                                  length="120" rows="2"></textarea>
                                        <label for="comment">Comentario</label>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="md-form">
                                        <div class="file-field">
                                            <div class="file-field">
                                                <a class="btn-floating central mt-0 float-left">
                                                    <i class="fas fa-paperclip" aria-hidden="true"></i>
                                                    <input type="file" name="foto" id="foto" accept="image/*">
                                                </a>
                                                <div class="file-path-wrapper">
                                                    <input class="file-path validate" name="fotos" id="fotos" accept="image/*" type="text" placeholder="Carga tu foto">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <button type="submit"
                                            class="btn btn-outline-default  btn-rounded waves-effect btn-block">
                                        Timbrar
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')

@endpush
