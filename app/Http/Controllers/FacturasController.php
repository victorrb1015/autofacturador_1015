<?php

namespace App\Http\Controllers;


use App\AutoFact;
use App\AutofTicket;
use Illuminate\Support\Facades\DB;
use App\Org;
use App\Park;
use function Composer\Autoload\includeFile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use RealRashid\SweetAlert\Facades\Alert;
use GuzzleHttp\Client;
use http\Client\Curl\User;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use \CfdiUtils\XmlResolver\XmlResolver;
use \CfdiUtils\CadenaOrigen\DOMBuilder;
use Endroid\QrCode\QrCode;

class FacturasController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $hoy = date('Y-m-d');
        $fact = AutoFact::join('autof_tickets', 'auto_facts.id_ticketAF', '=', 'autof_tickets.id')
            //->whereDate('fecha_timbrado','=',$hoy)
            ->get();
        return
            view('facturas.facturas', compact('fact'));
    }

    public function nueva()
    {

        return view('facturas.estacionamiento');
    }

    public function fact()
    {

        $no_est = request()->validate([
            'no_est' => ['required', 'integer'],
        ]);
        $data = DB::table('parks')->join('orgs', 'orgs.id', '=', 'parks.id_org')->where('no_est', '=', $no_est['no_est'])->get();
        return view('facturas.crear', [
            'park' => $data[0],
        ]);
    }

    public function rechazada()
    {
        $hoy = date('Y-m-d');
        $tickets = AutofTicket::where('estatus', '=', 'Rechazo')
            ->get();
        return view('facturas.rechazada', compact('tickets'));
    }

    public function log()
    {
        $fact = AutoFact::join('autof_tickets', 'auto_facts.id_ticketAF', '=', 'autof_tickets.id')->get();
        return view('facturas.log', compact('fact'));
    }

    public function timb()
    {
        $img ='';
        $fact = request()->validate([
            'park' => ['required', 'numeric'],
            'RS' => ['required', 'string'],
            'RFC' => ['required', 'string', 'max:13'],
            'email' => ['required', 'string', 'email'],
            'usoCFDI' => ['required', 'string', 'max:3'],
            'moneda' => ['required', 'string', 'max:3'],
            'forma_pago' => ['required', 'string', 'max:2'],
            'metodo_pago' => ['required', 'string', 'max:3'],
            'concepto' => ['required', 'string', 'max:1'],
            'cantidad' => ['required', 'numeric'],
            'precio' => ['required', 'string'],
            'comment' => ['required','string']
        ]);
        $foto = request()->file('foto');
        if($foto != null){
            $dataImg = file_get_contents($foto->getLinkTarget());
            $img = base64_encode($dataImg);
        }
        $fecha = date('Y-m-d');
        $fecha2 = date('H:i:s');
        $numfact = AutofTicket::all()->max('factura');
        $ticket = new AutofTicket();
        $ticket->id_est = $fact['park'];
        $ticket->factura = $numfact + 1;
        $ticket->total_ticket = $fact['precio'];
        $ticket->fecha_emision = $fecha;
        $ticket->estatus = 'valido';
        $ticket->UsoCFDI = $fact['usoCFDI'];
        $ticket->metodo_pago = $fact['metodo_pago'];
        $ticket->forma_pago = $fact['forma_pago'];
        $ticket->RFC = trim($fact['RFC']);
        $ticket->Razon_social = $fact['RS'];
        $ticket->email = $fact['email'];
        $ticket->email = $fact['email'];
        $ticket->id_cliente = 1;
        $ticket->id_tipo = 1;
        $ticket->id_CentralUser = 1;
        $ticket->coment = $fact['comment'];
        if($foto != null){
            $ticket->imagen = $img;
        }
        $ticket->save();

        $est = Park::where('no_est', '=', $fact['park'])->get();
        $org = Org::find($est[0]['id_org']);

        $certificado = new \CfdiUtils\Certificado\Certificado('doc/00001000000402646722.cer');
        $comprobanteAtributos = [
            'xmlns:cfdi' => 'http://www.sat.gob.mx/cfd/3',
            'xmlns:xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
            'LugarExpedicion' => $est[0]['cp'],
            'MetodoPago' => $ticket->metodo_pago,
            'TipoDeComprobante' => 'I',
            'Total' => number_format($ticket->total_ticket, 2),
            'Moneda' => 'MXN',
            'SubTotal' => number_format($ticket->total_ticket - ($ticket->total_ticket * .16), 2),
            'FormaPago' => $ticket->forma_pago,
            'Fecha' => $fecha . "T" . $fecha2,
            'Folio' => $est[0]['folio'] + 1,
            'Serie' => $est[0]['serie'],
            'Version' => '3.3',
            'xsi:schemaLocation' => 'http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv33.xsd',
        ];
        $creator = new \CfdiUtils\CfdiCreator33($comprobanteAtributos, $certificado);
        $comprobante = $creator->comprobante();
        $comprobante->addEmisor([
            'RegimenFiscal' => $org['Regimen_fiscal'],
            'Nombre' => $org['Razon_social'],
            'Rfc' => $org['RFC'],
        ]);
        $comprobante->addReceptor([
            'UsoCFDI' => $ticket->UsoCFDI,
            'Nombre' => $ticket->Razon_social,
            'Rfc' => $ticket->RFC,
        ]);
        $comprobante->addConcepto([
            'ClaveProdServ' => '78111807',
            'Cantidad' => '1.00',
            'ClaveUnidad' => 'E48',
            'Unidad' => 'Unidad de servicio',
            'Descripcion' => 'Tarifas del Parqueadero',
            'ValorUnitario' => str_replace(",", "", number_format($ticket->total_ticket - ($ticket->total_ticket * .16), 2)),
            'Importe' => str_replace(",", "", number_format($ticket->total_ticket - ($ticket->total_ticket * .16), 2)),
        ])->addTraslado([
            'Importe' => str_replace(",", "", number_format($ticket->total_ticket * .16, 2)),
            'TasaOCuota' => '0.160000',
            'TipoFactor' => 'Tasa',
            'Impuesto' => '002',
            'Base' => str_replace(",", "", number_format($ticket->total_ticket, 2)),
        ]);
        $creator->addSumasConceptos(NULL, 2);
        $key = file_get_contents('doc/CSD_OPERADORA_CENTRAL_DE_ESTACIONAMIENTOS_SAPI_DE_CV_OCE9412073L3_20160520_091056.key.pem');
        $creator->addSello($key, 'oce94120');
        //$creator->saveXml($est[0]['serie'] . ($est[0]['folio'] + 1) . '.xml');
        $xml = $creator->asXml();
        $xml2 = base64_encode($xml);
        $body = array(
            'xml' => $xml2,
        );
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api-stage.emite.dev/v1/stamp",//desarrollo
            //CURLOPT_URL => "https://api.emite.app/v1/stamp",//produccion
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($body),
            CURLOPT_HTTPHEADER => array(
                "Accept: application/json; charset=UTF-8",
                "Authorization: bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJCNG5HdWEtbkZ1S1ZkaDhHN1pYUmdtTXE1LXdQeEpNTmhRTEMxb0hKIiwic3ViIjoiQ2VudHJhbCBbU1RBR0VdIiwiaXNzIjoiRU1JVEUgW1NUQUdFXSIsImF1ZCI6IkJFTkdBTEEgW1NUQUdFXSJ9.p6lmdlIGKB5Z2FedsUusN8U3CkY5sEUcwz4BHx_VBIM",//desarrollo
                //"Authorization: bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJUVExlaTVwWW52RHNQVWF1elkyci1rTDJaTmhNUHk3dWNYV3RrYUFYIiwic3ViIjoiSW50ZWdyYWRvci1UZWNobm9sb2d5IFtQUk9EXSIsImlzcyI6IkVNSVRFIFtQUk9EXSIsImF1ZCI6IkJFTkdBTEEgW1BST0RdIn0.MMHzeCtRo3E8rTLnP4bsaCSh7m13_u4MlZ7E23MXZCI",//produccion
                "Content-Type: application/json; charset=UTF-8"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);
        if ($err) {
            return $err;
        } else {
            $val = json_decode($response);
            if ($val->data->status == "previously_stamped" || $val->data->status == "stamped") {
                $xml3 = $val->data->document_info->xml;
                $uuid = $val->data->stamp_info->uuid;
                $xml3 = base64_decode($xml3);
                file_put_contents("Timbrados/" . $uuid . ".xml", $xml3);
                $xmlContent = file_get_contents("Timbrados/" . $uuid . ".xml");
                $resolver = new XmlResolver();
                $location = $resolver->resolveCadenaOrigenLocation('3.3');
                $builder = new DOMBuilder();
                $cadenaorigen = $builder->build($xmlContent, $location);

                $xmlContents = $xml3;
                $cfdi = \CfdiUtils\Cfdi::newFromString($xmlContents);
                $cfdi->getVersion(); // (string) 3.3
                $cfdi->getDocument(); // clon del objeto DOMDocument
                $cfdi->getSource(); // (string) <cfdi:Comprobante...
                $complemento = $cfdi->getNode(); // Nodo de trabajo del nodo cfdi:Comprobante
                $tfd = $complemento->searchNode('cfdi:Complemento', 'tfd:TimbreFiscalDigital');
                //return var_dump($tfd);
                $data = array(
                    'org' => [
                        'RS' => $org['Razon_social'],
                        'RFC' => $org['RFC'],
                    ],
                    'client' => [
                        'RS' => $ticket->Razon_social,
                        'RFC' => $ticket->RFC,
                    ],
                    'fact' => [
                        'SF' => $est[0]['serie'] . ($est[0]['folio'] + 1),
                        'FF' => $uuid,
                        'CSD' => $complemento['NoCertificado'],
                        'FHE' => $complemento['Fecha'],
                        'FHC' => $tfd['FechaTimbrado'],
                        'NSAT' => $tfd['NoCertificadoSAT'],
                        'UC' => $ticket->UsoCFDI,
                        'MP' => $ticket->metodo_pago,
                        'FP' => $ticket->forma_pago,
                        'RF' => $org['Regimen_fiscal'],
                    ],
                    'con' => [
                        'CA' => '1.00',
                        'UN' => 'Unidad de servicio',
                        'PU' => number_format($ticket->total_ticket - ($ticket->total_ticket * .16), 2),
                        'DE' => '0.00',
                        'IM' => number_format($ticket->total_ticket, 2),
                        'IVA' => number_format($ticket->total_ticket - ($ticket->total_ticket * .16), 2),
                        'IEPS' => '0.00',
                        'IMP_IEPS' => '0.00',
                    ],
                    'TT' => [
                        'T' => number_format($ticket->total_ticket, 2),
                        'ST' => number_format($ticket->total_ticket - ($ticket->total_ticket * .16), 2),
                        'IVA' => number_format($ticket->total_ticket - ($ticket->total_ticket * .16), 2),
                    ],
                    'FC' => [
                        'CO' => $cadenaorigen,
                        'SSAT' => wordwrap($tfd['SelloSAT'],120,"\n",true),
                        'SCFD' => wordwrap($tfd['SelloCFD'],120,"\n",true),
                    ]
                );
                $ochocarct = substr($uuid, -8);
                $qrCode = new QrCode('https://verificacfdi.facturaelectronica.sat.gob.mx/default.aspx&id=' . $uuid . '&r' . $org['RFC'] . '&rr=' . $ticket->RFC . '&tt=' . number_format($ticket->total_ticket, 2) . '0000');
                header('Content-Type: ' . $qrCode->getContentType());
                $qrCode->writeString();
                $ubiQr = "pdf-template/" . $uuid . "qrcode.png";
                $qrCode->writeFile($ubiQr);
                \PDF::loadView('PDF.pdf', ['data' => $data])->save("Timbrados/" . $uuid . '.pdf');
                //$pdf->stream();
                //file_put_contents("Timbrados/" .$uuid . '.pdf',$pdf);
                $mail = new PHPMailer(true);
                $mail->From = "no-reply@centralmx.tech";
                $mail->FromName = "Central operadora de estacionamientos";
                $mail->Subject = "Factura" . $est[0]['serie'] . ($est[0]['folio'] + 1);
                $mail->Body = "<html><h4 style='font-color: green;' align='center';>Central Operadora de Estacionamientos SAPI S.A. de C.V.</h4>
                <p align='center;'>Envía a usted el archivo XML correspondiente al Comprobante Fiscal Digital con Folio Fiscal: " . $uuid . " , Serie: " . $est[0]['serie'] . " y Folio: " . ($est[0]['folio'] + 1) . ". Así como su representación impresa.</p>
                <p align='center;'>Este correo electrónico ha sido generado automáticamente por el Sistema de Emisión de Comprobantes Digitales por lo que le solicitamos no responder a este mensaje, ya que las respuestas a este correo electrónico no serán leídas. En caso de tener alguna duda referente a la información contenida en el Comprobante Fiscal Digital contacte a Central Operadora de Estacionamientos SAPI S.A. de C.V. para su aclaración.</p>
                <p align='center;'>Está recibiendo este correo electrónico debido a que ha proporcionado la dirección de correo electrónico " . $ticket->email . " a Central Operadora de Estacionamientos SAPI S.A. de C.V. para hacerle llegar su Factura Electrónica.!</p></html>";
                $mail->AddAddress($ticket->email, $ticket->Razon_social);
                $archivo = "Timbrados/" . $uuid . ".xml";
                $pdf1 = "Timbrados/" . $uuid . '.pdf';
                $mail->AddAttachment($archivo);
                $mail->AddAttachment($pdf1);
                $mail->IsHTML(true);
                $mail->Send();
                $contenido = file_get_contents("Timbrados/" . $uuid . '.pdf');
                $ticket->estatus = "SAT";
                $ticket->save();
                DB::table('parks')->where('no_est', '=', $est[0]['no_est'])->update(['folio' => $est[0]['folio'] + 1]);
                $factura = new AutoFact();
                $factura->serie = $est[0]['serie'];
                $factura->tipo_doc = "I";
                $factura->folio = $est[0]['folio'] + 1;
                $factura->subtotal_factura = str_replace(",", "", number_format($ticket->total_ticket - ($ticket->total_ticket * .16), 2));
                $factura->iva_factura = str_replace(",", "", number_format(($ticket->total_ticket - ($ticket->total_ticket * .16)) * .16, 2));
                $factura->total_factura = str_replace(",", "", number_format($ticket->total_ticket, 2));
                $factura->XML = $xml3;
                $factura->PDF = base64_encode($contenido);
                $factura->estatus = "timbrada";
                $factura->id_ticketAF = $ticket->id;
                $factura->fecha_timbrado = date("Y-m-d H:i:s");
                $factura->uuid = $uuid;
                $factura->save();
                Alert::success('Valida', 'Comprobante timbrado');
                //unlink($est[0]['serie'] . ($est[0]['folio'] + 1) . '.xml');
                unlink($archivo);
                unlink($pdf1);
                unlink($ubiQr);
                return $this->nueva();

            } else {
                /*;
                return $this->index();*/
                $val2 = json_decode($response);
                if ($val2->data->count === 1) {
                    //return var_dump($val2->data->details[0]);
                    Alert::error('[' . $val2->data->details[0]->code . '] ' . $val2->data->details[0]->description, $val2->data->details[0]->validation);
                    return $this->nueva();
                }
                return $response;

            }
        }
    }

    public function reenviar(AutoFact $fact){
        $uuid = $fact->uuid;
        $xml3 = $fact->XML;
        $pdf = base64_decode($fact->PDF);
        $ticket = AutofTicket::find($fact->id_ticketAF);
        file_put_contents("Timbrados/" . $uuid . ".pdf", $pdf);
        file_put_contents("Timbrados/" . $uuid . ".xml", $xml3);
        $mail = new PHPMailer(true);
        $mail->From = "no-reply@centralmx.tech";
        $mail->FromName = "Central operadora de estacionamientos";
        $mail->Subject = "Factura" . $fact->serie . $fact->folio;
        $mail->Body = "<html><h4 style='font-color: green;' align='center';>Central Operadora de Estacionamientos SAPI S.A. de C.V.</h4>
                <p align='center;'>Envía a usted el archivo XML correspondiente al Comprobante Fiscal Digital con Folio Fiscal: " . $uuid . " , Serie: " . $fact->serie . " y Folio: " . $fact->folio . ". Así como su representación impresa.</p>
                <p align='center;'>Este correo electrónico ha sido generado automáticamente por el Sistema de Emisión de Comprobantes Digitales por lo que le solicitamos no responder a este mensaje, ya que las respuestas a este correo electrónico no serán leídas. En caso de tener alguna duda referente a la información contenida en el Comprobante Fiscal Digital contacte a Central Operadora de Estacionamientos SAPI S.A. de C.V. para su aclaración.</p>
                <p align='center;'>Está recibiendo este correo electrónico debido a que ha proporcionado la dirección de correo electrónico " . $ticket->email . " a Central Operadora de Estacionamientos SAPI S.A. de C.V. para hacerle llegar su Factura Electrónica.!</p></html>";
        $mail->AddAddress($ticket->email, $ticket->Razon_social);
        $archivo = "Timbrados/" . $uuid . ".xml";
        $pdf1 = "Timbrados/" . $uuid . '.pdf';
        $mail->AddAttachment($archivo);
        $mail->AddAttachment($pdf1);
        $mail->IsHTML(true);
        $mail->Send();
        Alert::success('Reenviado', 'Comprobante fue reenviado correctamente');
        return $this->index();
    }

}
