<?php

namespace App\Http\Controllers;

use App\AutoFact;
use App\AutofTicket;
use App\Mail\RechazoMail;
use App\Org;
use App\Park;
use function Composer\Autoload\includeFile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use RealRashid\SweetAlert\Facades\Alert;
use GuzzleHttp\Client;
use http\Client\Curl\User;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use \CfdiUtils\XmlResolver\XmlResolver;
use \CfdiUtils\CadenaOrigen\DOMBuilder;
use Endroid\QrCode\QrCode;

class TicketsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function ticket()
    {
        $tickets = AutofTicket::all();
        return view('Tickets.ticket', compact('tickets'));
    }

    public function validar()
    {
        $tickets = DB::table('autof_tickets')
            ->join('parks', 'autof_tickets.id_est', '=', 'parks.no_est')
            ->where('estatus', '=', 'validar')
            ->get();
        return view('Tickets.validar', compact('tickets'));
    }

    public function validar_asc()
    {
        $tickets = DB::table('autof_tickets')
            ->join('parks', 'autof_tickets.id_est', '=', 'parks.no_est')
            ->where('estatus', '=', 'validar')
            ->get();
        foreach ($tickets as $a) {
            echo $a;
        }
        return view('Tickets.validar', compact('tickets'));
    }

    public function sin_est()
    {
        $tickets = DB::table('autof_tickets')
            ->join('parks', 'autof_tickets.id_est', '=', 'parks.no_est')
            ->where('estatus', '=', 'sin_fact')
            ->orderBy('fecha_emision')
            ->get();
        return view('Tickets.sin_est', compact('tickets'));
    }

    public function rechazada()
    {
        $tickets = DB::table('autof_tickets')
            ->join('parks', 'autof_tickets.id_est', '=', 'parks.no_est')
            ->where('estatus', '=', 'Rechazo')
            ->get();
        return view('Tickets.rechazo', compact('tickets'));
    }

    public function rechazo()
    {
        $coment = request()->validate([
            'id' => 'required',
            'motivo' => 'required',
        ]);
        $ticket = AutofTicket::find($coment['id']);
        if ($ticket->id >= 1) {
            $ticket->estatus = 'Rechazo';
            $ticket->coment = $coment['motivo'];
            $ticket->save();
            // Se manda correo
            $mail = new PHPMailer(true);
            $mail->From = "no-reply@centralmx.tech";
            $mail->FromName = "Central operadora de estacionamientos";
            $mail->Subject = "Rechazo Factura";
            $mail->Body = "<html><h4 style='font-color: green;' align='center';>Central Operadora de Estacionamientos SAPI S.A. de C.V.</h4>
 
<p align='center;'>Esta rechazando su factura por " . $coment['motivo'] . "</p>

<p align='center;'>Este correo electrónico ha sido generado automáticamente por el Sistema de Emisión de Comprobantes Digitales por lo que le solicitamos no responder a este mensaje, ya que las respuestas a este correo electrónico no serán leídas. En caso de tener alguna duda referente a la información contenida en el Comprobante Fiscal Digital contacte a Central Operadora de Estacionamientos SAPI S.A. de C.V. para su aclaración.</p>

<p align='center;'>Está recibiendo este correo electrónico debido a que ha proporcionado la dirección de correo electrónico " . $ticket->email . " a Central Operadora de Estacionamientos SAPI S.A. de C.V. para hacerle llegar su Factura Electrónica.!</p></html>";
            $mail->AddAddress($ticket->email, $ticket->Razon_social);
            $mail->IsHTML(true);
            $mail->Send();
            Alert::success('Rechazo', 'Se rechazo correctamente el ticket');
        } else {
            Alert::error('Error', 'No se pudo rechazar el ticket');
        }
        $tickets = AutofTicket::where('estatus', '=', 'validar')->get();
        return view('Tickets.validar', compact('tickets'));
    }

    public function valido(AutofTicket $ticket)
    {
        $valido = DB::table('autof_tickets')
            ->where('no_ticket', '=', $ticket->no_ticket)
            ->where('total_ticket', '=', $ticket->total_ticket)
            ->where('fecha_emision', '=', $ticket->fecha_emision)
            ->where('id_est', '=', $ticket->id_est)
            ->get();
        if (count($valido) != 1) {
            Alert::error('Error', 'Un ticket ya fue facturado con la misma información de folio, total y fecha para este estacionameinto');
            return $this->validar();
        }
        $est = Park::where('no_est', '=', $ticket['id_est'])->get();
        $org = Org::find($est[0]['id_org']);
        $fecha = date('Y-m-d');
        $fecha2 = date('H:i:s');
        $certificado = new \CfdiUtils\Certificado\Certificado('doc/00001000000402646722.cer');
        $comprobanteAtributos = [
            'xmlns:cfdi' => 'http://www.sat.gob.mx/cfd/3',
            'xmlns:xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
            'LugarExpedicion' => $est[0]['cp'],
            'MetodoPago' => $ticket['metodo_pago'],
            'TipoDeComprobante' => 'I',
            'Total' => number_format($ticket['total_ticket'], 2),
            'Moneda' => 'MXN',
            'SubTotal' => number_format($ticket['total_ticket'] - ($ticket['total_ticket'] * .16), 2),
            'FormaPago' => $ticket['forma_pago'],
            'Fecha' => $fecha . "T" . $fecha2,
            'Folio' => $est[0]['folio'] + 1,
            'Serie' => $est[0]['serie'],
            'Version' => '3.3',
            'xsi:schemaLocation' => 'http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv33.xsd',
        ];
        $creator = new \CfdiUtils\CfdiCreator33($comprobanteAtributos, $certificado);
        $comprobante = $creator->comprobante();
        $comprobante->addEmisor([
            'RegimenFiscal' => $org['Regimen_fiscal'],
            'Nombre' => $org['Razon_social'],
            'Rfc' => $org['RFC'],
        ]);
        $comprobante->addReceptor([
            'UsoCFDI' => $ticket['UsoCFDI'],
            'Nombre' => $ticket['Razon_social'],
            'Rfc' => trim($ticket['RFC']),
        ]);
        $comprobante->addConcepto([
            'ClaveProdServ' => '78111807',
            'Cantidad' => '1.00',
            'ClaveUnidad' => 'E48',
            'Unidad' => 'Unidad de servicio',
            'Descripcion' => 'Tarifas del Parqueadero',
            'ValorUnitario' => str_replace(",", "", number_format($ticket['total_ticket'] - ($ticket['total_ticket'] * .16), 2)),
            'Importe' => str_replace(",", "", number_format($ticket['total_ticket'] - ($ticket['total_ticket'] * .16), 2)),
        ])->addTraslado([
            'Importe' => str_replace(",", "", number_format($ticket['total_ticket'] * .16, 2)),
            'TasaOCuota' => '0.160000',
            'TipoFactor' => 'Tasa',
            'Impuesto' => '002',
            'Base' => str_replace(",", "", number_format($ticket['total_ticket'], 2)),
        ]);
        $creator->addSumasConceptos(NULL, 2);
        $key = file_get_contents('doc/CSD_OPERADORA_CENTRAL_DE_ESTACIONAMIENTOS_SAPI_DE_CV_OCE9412073L3_20160520_091056.key.pem');
        $creator->addSello($key, 'oce94120');
        //$creator->saveXml($est[0]['serie'] . ($est[0]['folio'] + 1) . '.xml');
        $xml = $creator->asXml();
        $xml2 = base64_encode($xml);
        $body = array(
            'xml' => $xml2,
        );
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api-stage.emite.dev/v1/stamp",//desarrollo
            //CURLOPT_URL => "https://api.emite.app/v1/stamp",//produccion
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($body),
            CURLOPT_HTTPHEADER => array(
                "Accept: application/json; charset=UTF-8",
                "Authorization: bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJCNG5HdWEtbkZ1S1ZkaDhHN1pYUmdtTXE1LXdQeEpNTmhRTEMxb0hKIiwic3ViIjoiQ2VudHJhbCBbU1RBR0VdIiwiaXNzIjoiRU1JVEUgW1NUQUdFXSIsImF1ZCI6IkJFTkdBTEEgW1NUQUdFXSJ9.p6lmdlIGKB5Z2FedsUusN8U3CkY5sEUcwz4BHx_VBIM",//desarrollo
                //"Authorization: bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJUVExlaTVwWW52RHNQVWF1elkyci1rTDJaTmhNUHk3dWNYV3RrYUFYIiwic3ViIjoiSW50ZWdyYWRvci1UZWNobm9sb2d5IFtQUk9EXSIsImlzcyI6IkVNSVRFIFtQUk9EXSIsImF1ZCI6IkJFTkdBTEEgW1BST0RdIn0.MMHzeCtRo3E8rTLnP4bsaCSh7m13_u4MlZ7E23MXZCI",//produccion
                "Content-Type: application/json; charset=UTF-8"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);
        if ($err) {
            return $err;
        } else {
            $val = json_decode($response);
            if ($val->data->status == "previously_stamped" || $val->data->status == "stamped") {
                $xml3 = $val->data->document_info->xml;
                $uuid = $val->data->stamp_info->uuid;
                $xml3 = base64_decode($xml3);
                file_put_contents("Timbrados/" . $uuid . ".xml", $xml3);
                $xmlContent = file_get_contents("Timbrados/" . $uuid . ".xml");
                $resolver = new XmlResolver();
                $location = $resolver->resolveCadenaOrigenLocation('3.3');
                $builder = new DOMBuilder();
                $cadenaorigen = $builder->build($xmlContent, $location);
                /**/
                $xmlContents = $xml3;
                $cfdi = \CfdiUtils\Cfdi::newFromString($xmlContents);
                $cfdi->getVersion(); // (string) 3.3
                $cfdi->getDocument(); // clon del objeto DOMDocument
                $cfdi->getSource(); // (string) <cfdi:Comprobante...
                $complemento = $cfdi->getNode(); // Nodo de trabajo del nodo cfdi:Comprobante
                $tfd = $complemento->searchNode('cfdi:Complemento', 'tfd:TimbreFiscalDigital');
                //return var_dump($tfd);
                $data = array(
                    'org' => [
                        'RS' => $org['Razon_social'],
                        'RFC' => $org['RFC'],
                    ],
                    'client' => [
                        'RS' => $ticket['Razon_social'],
                        'RFC' => trim($ticket['RFC']),
                    ],
                    'fact' => [
                        'SF' => $est[0]['serie'] . ($est[0]['folio'] + 1),
                        'FF' => $uuid,
                        'CSD' => $complemento['NoCertificado'],
                        'FHE' => $complemento['Fecha'],
                        'FHC' => $tfd['FechaTimbrado'],
                        'NSAT' => $tfd['NoCertificadoSAT'],
                        'UC' => $ticket['UsoCFDI'],
                        'MP' => $ticket['metodo_pago'],
                        'FP' => $ticket['forma_pago'],
                        'RF' => $org['Regimen_fiscal'],
                    ],
                    'con' => [
                        'CA' => '1.00',
                        'UN' => 'Unidad de servicio',
                        'PU' => number_format($ticket['total_ticket'] - ($ticket['total_ticket'] * .16), 2),
                        'DE' => '0.00',
                        'IM' => number_format($ticket['total_ticket'], 2),
                        'IVA' => number_format($ticket['total_ticket'] * .16, 2),
                        'IEPS' => '0.00',
                        'IMP_IEPS' => '0.00',
                    ],
                    'TT' => [
                        'T' => number_format($ticket['total_ticket'], 2),
                        'ST' => number_format($ticket['total_ticket'] - ($ticket['total_ticket'] * .16), 2),
                        'IVA' => number_format($ticket['total_ticket'] * .16, 2),
                    ],
                    'FC' => [
                        'CO' => $cadenaorigen,
                        'SSAT' => wordwrap($tfd['SelloSAT'], 120, "\n", true),
                        'SCFD' => wordwrap($tfd['SelloCFD'], 120, "\n", true),
                    ]
                );
                $ochocarct = substr($uuid, -8);
                $qrCode = new QrCode('https://verificacfdi.facturaelectronica.sat.gob.mx/default.aspx&id=' . $uuid . '&r' . $org['RFC'] . '&rr=' . $ticket['RFC'] . '&tt=' . number_format($ticket['total_ticket'], 2) . '0000');
                header('Content-Type: ' . $qrCode->getContentType());
                $qrCode->writeString();
                $ubiQr = "pdf-template/" . $uuid . "qrcode.png";
                $qrCode->writeFile($ubiQr);
                \PDF::loadView('PDF.pdf', ['data' => $data])->save("Timbrados/" . $uuid . '.pdf');
                //$pdf->stream();
                //file_put_contents("Timbrados/" .$uuid . '.pdf',$pdf);
                $mail = new PHPMailer(true);
                $mail->From = "no-reply@centralmx.tech";
                $mail->FromName = "Central operadora de estacionamientos";
                $mail->Subject = "Factura" . $est[0]['serie'] . ($est[0]['folio'] + 1);
                $mail->Body = "<html><h4 style='font-color: green;' align='center';>Central Operadora de Estacionamientos SAPI S.A. de C.V.</h4>
                <p align='center;'>Envía a usted el archivo XML correspondiente al Comprobante Fiscal Digital con Folio Fiscal: " . $uuid . " , Serie: " . $est[0]['serie'] . " y Folio: " . ($est[0]['folio'] + 1) . ". Así como su representación impresa.</p>
                <p align='center;'>Este correo electrónico ha sido generado automáticamente por el Sistema de Emisión de Comprobantes Digitales por lo que le solicitamos no responder a este mensaje, ya que las respuestas a este correo electrónico no serán leídas. En caso de tener alguna duda referente a la información contenida en el Comprobante Fiscal Digital contacte a Central Operadora de Estacionamientos SAPI S.A. de C.V. para su aclaración.</p>
                <p align='center;'>Está recibiendo este correo electrónico debido a que ha proporcionado la dirección de correo electrónico " . $ticket['email'] . " a Central Operadora de Estacionamientos SAPI S.A. de C.V. para hacerle llegar su Factura Electrónica.!</p></html>";
                $mail->AddAddress($ticket['email'], $ticket['Razon_social']);
                $archivo = "Timbrados/" . $uuid . ".xml";
                $pdf1 = "Timbrados/" . $uuid . '.pdf';
                $mail->AddAttachment($archivo);
                $mail->AddAttachment($pdf1);
                $mail->IsHTML(true);
                $mail->Send();
                $contenido = file_get_contents("Timbrados/" . $uuid . '.pdf');
                $ticket->estatus = "valido";
                $ticket->save();
                DB::table('parks')->where('no_est', '=', $est[0]['no_est'])->update(['folio' => $est[0]['folio'] + 1]);
                $factura = new AutoFact();
                $factura->serie = $est[0]['serie'];
                $factura->tipo_doc = "I";
                $factura->folio = $est[0]['folio'] + 1;
                $factura->subtotal_factura = str_replace(",", "", number_format($ticket['total_ticket'] - ($ticket['total_ticket'] * .16), 2));
                $factura->iva_factura = str_replace(",", "", number_format(($ticket['total_ticket'] - ($ticket['total_ticket'] * .16)) * .16, 2));
                $factura->total_factura = str_replace(",", "", number_format($ticket['total_ticket'], 2));
                $factura->XML = $xml3;
                $factura->PDF = base64_encode($contenido);
                $factura->estatus = "timbrada";
                $factura->id_ticketAF = $ticket['id'];
                $factura->fecha_timbrado = date("Y-m-d H:i:s");
                $factura->uuid = $uuid;
                $factura->save();
                Alert::success('Valida', 'Comprobante timbrado');
                //unlink($est[0]['serie'] . ($est[0]['folio'] + 1) . '.xml');
                unlink($archivo);
                unlink($pdf1);
                unlink($ubiQr);
                return $this->validar();
                //*/
            } else {
                /*;
                return $this->index();*/
                $val2 = json_decode($response);
                if ($val2->data->count === 1) {
                    //return var_dump($val2->data->details[0]);
                    Alert::error('[' . $val2->data->details[0]->code . '] ' . $val2->data->details[0]->description, $val2->data->details[0]->validation);
                    return $this->validar();
                }
                return $response;
            }
        }
        /* */
    }

    public function actualiza_estacionamiento1()
    {
        $res = request()->validate([
            'no_est' => ['required', 'numeric'],
            'ticket' => ['required', 'numeric']
        ]);
        $est = Park::where('no_est', '=', $res['no_est'])->get();
        if (empty($est[0])) {
            Alert::error('Error', 'Error el estacionamiento no existe en el sistema');
            return $this->validar();
        } else {
            $ticket = AutofTicket::find($res['ticket']);
            $ticket->id_est = $res['no_est'];
            $ticket->save();
            Alert::success('Valido', 'Se ah actualizado el estacionameinto');
            return $this->validar();
        }
    }

    public function actualiza_estacionamiento2()
    {
        $res = request()->validate([
            'no_est' => ['required', 'numeric'],
            'ticket' => ['required', 'numeric']
        ]);
        $est = Park::where('no_est', '=', $res['no_est'])->get();
        if (empty($est[0])) {
            Alert::error('Error', 'Error el estacionamiento no existe en el sistema');
            return $this->sin_est();
        } else {
            $ticket = AutofTicket::find($res['ticket']);
            $ticket->id_est = $res['no_est'];
            $ticket->save();
            Alert::success('Valido', 'Se ah actualizado el estacionameinto');
            return $this->sin_est();
        }
    }

    public function valido2(AutofTicket $ticket)
    {
        $valido = DB::table('autof_tickets')
            ->where('no_ticket', '=', $ticket->no_ticket)
            ->where('total_ticket', '=', $ticket->total_ticket)
            ->where('fecha_emision', '=', $ticket->fecha_emision)
            ->where('id_est', '=', $ticket->id_est)
            ->get();
        if (count($valido) != 1) {
            Alert::error('Error', 'Un ticket ya fue facturado con la misma información de folio, total y fecha para este estacionameinto');
            return $this->validar();
        }
        $est = Park::where('no_est', '=', $ticket['id_est'])->get();
        $org = Org::find($est[0]['id_org']);
        $fecha = date('Y-m-d');
        $fecha2 = date('H:i:s');
        $certificado = new \CfdiUtils\Certificado\Certificado('doc/00001000000402646722.cer');
        $comprobanteAtributos = [
            'xmlns:cfdi' => 'http://www.sat.gob.mx/cfd/3',
            'xmlns:xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
            'LugarExpedicion' => $est[0]['cp'],
            'MetodoPago' => $ticket['metodo_pago'],
            'TipoDeComprobante' => 'I',
            'Total' => number_format($ticket['total_ticket'], 2),
            'Moneda' => 'MXN',
            'SubTotal' => number_format($ticket['total_ticket'] - ($ticket['total_ticket'] * .16), 2),
            'FormaPago' => $ticket['forma_pago'],
            'Fecha' => $fecha . "T" . $fecha2,
            'Folio' => $est[0]['folio'] + 1,
            'Serie' => $est[0]['serie'],
            'Version' => '3.3',
            'xsi:schemaLocation' => 'http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv33.xsd',
        ];
        $creator = new \CfdiUtils\CfdiCreator33($comprobanteAtributos, $certificado);
        $comprobante = $creator->comprobante();
        $comprobante->addEmisor([
            'RegimenFiscal' => $org['Regimen_fiscal'],
            'Nombre' => $org['Razon_social'],
            'Rfc' => $org['RFC'],
        ]);
        $comprobante->addReceptor([
            'UsoCFDI' => $ticket['UsoCFDI'],
            'Nombre' => $ticket['Razon_social'],
            'Rfc' => trim($ticket['RFC']),
        ]);
        $comprobante->addConcepto([
            'ClaveProdServ' => '78111807',
            'Cantidad' => '1.00',
            'ClaveUnidad' => 'E48',
            'Unidad' => 'Unidad de servicio',
            'Descripcion' => 'Tarifas del Parqueadero',
            'ValorUnitario' => str_replace(",", "", number_format($ticket['total_ticket'] - ($ticket['total_ticket'] * .16), 2)),
            'Importe' => str_replace(",", "", number_format($ticket['total_ticket'] - ($ticket['total_ticket'] * .16), 2)),
        ])->addTraslado([
            'Importe' => str_replace(",", "", number_format($ticket['total_ticket'] * .16, 2)),
            'TasaOCuota' => '0.160000',
            'TipoFactor' => 'Tasa',
            'Impuesto' => '002',
            'Base' => str_replace(",", "", number_format($ticket['total_ticket'], 2)),
        ]);
        $creator->addSumasConceptos(NULL, 2);
        $key = file_get_contents('doc/CSD_OPERADORA_CENTRAL_DE_ESTACIONAMIENTOS_SAPI_DE_CV_OCE9412073L3_20160520_091056.key.pem');
        $creator->addSello($key, 'oce94120');
        $creator->saveXml($est[0]['serie'] . ($est[0]['folio'] + 1) . '.xml');
        $xml = $creator->asXml();
        $xml2 = base64_encode($xml);
        $body = array(
            'xml' => $xml2,
        );
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api-stage.emite.dev/v1/stamp",//desarrollo
            //CURLOPT_URL => "https://api.emite.app/v1/stamp",//produccion
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($body),
            CURLOPT_HTTPHEADER => array(
                "Accept: application/json; charset=UTF-8",
                "Authorization: bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJCNG5HdWEtbkZ1S1ZkaDhHN1pYUmdtTXE1LXdQeEpNTmhRTEMxb0hKIiwic3ViIjoiQ2VudHJhbCBbU1RBR0VdIiwiaXNzIjoiRU1JVEUgW1NUQUdFXSIsImF1ZCI6IkJFTkdBTEEgW1NUQUdFXSJ9.p6lmdlIGKB5Z2FedsUusN8U3CkY5sEUcwz4BHx_VBIM",//desarrollo
                //"Authorization: bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJUVExlaTVwWW52RHNQVWF1elkyci1rTDJaTmhNUHk3dWNYV3RrYUFYIiwic3ViIjoiSW50ZWdyYWRvci1UZWNobm9sb2d5IFtQUk9EXSIsImlzcyI6IkVNSVRFIFtQUk9EXSIsImF1ZCI6IkJFTkdBTEEgW1BST0RdIn0.MMHzeCtRo3E8rTLnP4bsaCSh7m13_u4MlZ7E23MXZCI",//produccion
                "Content-Type: application/json; charset=UTF-8"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);
        if ($err) {
            return $err;
        } else {
            $val = json_decode($response);
            if ($val->data->status == "previously_stamped" || $val->data->status == "stamped") {
                $xml3 = $val->data->document_info->xml;
                $uuid = $val->data->stamp_info->uuid;
                $xml3 = base64_decode($xml3);
                file_put_contents("Timbrados/" . $uuid . ".xml", $xml3);
                $xmlContent = file_get_contents("Timbrados/" . $uuid . ".xml");
                $resolver = new XmlResolver();
                $location = $resolver->resolveCadenaOrigenLocation('3.3');
                $builder = new DOMBuilder();
                $cadenaorigen = $builder->build($xmlContent, $location);
                /**/
                $xmlContents = $xml3;
                $cfdi = \CfdiUtils\Cfdi::newFromString($xmlContents);
                $cfdi->getVersion(); // (string) 3.3
                $cfdi->getDocument(); // clon del objeto DOMDocument
                $cfdi->getSource(); // (string) <cfdi:Comprobante...
                $complemento = $cfdi->getNode(); // Nodo de trabajo del nodo cfdi:Comprobante
                $tfd = $complemento->searchNode('cfdi:Complemento', 'tfd:TimbreFiscalDigital');
                //return var_dump($tfd);
                $data = array(
                    'org' => [
                        'RS' => $org['Razon_social'],
                        'RFC' => $org['RFC'],
                    ],
                    'client' => [
                        'RS' => $ticket['Razon_social'],
                        'RFC' => $ticket['RFC'],
                    ],
                    'fact' => [
                        'SF' => $est[0]['serie'] . ($est[0]['folio'] + 1),
                        'FF' => $uuid,
                        'CSD' => $complemento['NoCertificado'],
                        'FHE' => $complemento['Fecha'],
                        'FHC' => $tfd['FechaTimbrado'],
                        'NSAT' => $tfd['NoCertificadoSAT'],
                        'UC' => $ticket['UsoCFDI'],
                        'MP' => $ticket['metodo_pago'],
                        'FP' => $ticket['forma_pago'],
                        'RF' => $org['Regimen_fiscal'],
                    ],
                    'con' => [
                        'CA' => '1.00',
                        'UN' => 'Unidad de servicio',
                        'PU' => number_format($ticket['total_ticket'] - ($ticket['total_ticket'] * .16), 2),
                        'DE' => '0.00',
                        'IM' => number_format($ticket['total_ticket'], 2),
                        'IVA' => number_format($ticket['total_ticket'] * .16, 2),
                        'IEPS' => '0.00',
                        'IMP_IEPS' => '0.00',
                    ],
                    'TT' => [
                        'T' => number_format($ticket['total_ticket'], 2),
                        'ST' => number_format($ticket['total_ticket'] - ($ticket['total_ticket'] * .16), 2),
                        'IVA' => number_format($ticket['total_ticket'] * .16, 2),
                    ],
                    'FC' => [
                        'CO' => $cadenaorigen,
                        'SSAT' => wordwrap($tfd['SelloSAT'], 120, "\n", true),
                        'SCFD' => wordwrap($tfd['SelloCFD'], 120, "\n", true),
                    ]
                );
                $ochocarct = substr($uuid, -8);
                $qrCode = new QrCode('https://verificacfdi.facturaelectronica.sat.gob.mx/default.aspx&id=' . $uuid . '&r' . $org['RFC'] . '&rr=' . $ticket['RFC'] . '&tt=' . number_format($ticket['total_ticket'], 2) . '0000');
                header('Content-Type: ' . $qrCode->getContentType());
                $qrCode->writeString();
                $ubiQr = "pdf-template/" . $uuid . "qrcode.png";
                $qrCode->writeFile($ubiQr);
                \PDF::loadView('PDF.pdf', ['data' => $data])->save("Timbrados/" . $uuid . '.pdf');
                //$pdf->stream();
                //file_put_contents("Timbrados/" .$uuid . '.pdf',$pdf);
                $mail = new PHPMailer(true);
                $mail->From = "no-reply@centralmx.tech";
                $mail->FromName = "Central operadora de estacionamientos";
                $mail->Subject = "Factura" . $est[0]['serie'] . ($est[0]['folio'] + 1);
                $mail->Body = "<html><h4 style='font-color: green;' align='center';>Central Operadora de Estacionamientos SAPI S.A. de C.V.</h4>
                <p align='center;'>Envía a usted el archivo XML correspondiente al Comprobante Fiscal Digital con Folio Fiscal: " . $uuid . " , Serie: " . $est[0]['serie'] . " y Folio: " . ($est[0]['folio'] + 1) . ". Así como su representación impresa.</p>
                <p align='center;'>Este correo electrónico ha sido generado automáticamente por el Sistema de Emisión de Comprobantes Digitales por lo que le solicitamos no responder a este mensaje, ya que las respuestas a este correo electrónico no serán leídas. En caso de tener alguna duda referente a la información contenida en el Comprobante Fiscal Digital contacte a Central Operadora de Estacionamientos SAPI S.A. de C.V. para su aclaración.</p>
                <p align='center;'>Está recibiendo este correo electrónico debido a que ha proporcionado la dirección de correo electrónico " . $ticket['email'] . " a Central Operadora de Estacionamientos SAPI S.A. de C.V. para hacerle llegar su Factura Electrónica.!</p></html>";
                $mail->AddAddress($ticket['email'], $ticket['Razon_social']);
                $archivo = "Timbrados/" . $uuid . ".xml";
                $pdf1 = "Timbrados/" . $uuid . '.pdf';
                $mail->AddAttachment($archivo);
                $mail->AddAttachment($pdf1);
                $mail->IsHTML(true);
                $mail->Send();
                $contenido = file_get_contents("Timbrados/" . $uuid . '.pdf');
                $ticket->estatus = "valido";
                $ticket->save();
                DB::table('parks')->where('no_est', '=', $est[0]['no_est'])->update(['folio' => $est[0]['folio'] + 1]);
                $factura = new AutoFact();
                $factura->serie = $est[0]['serie'];
                $factura->tipo_doc = "I";
                $factura->folio = $est[0]['folio'] + 1;
                $factura->subtotal_factura = str_replace(",", "", number_format($ticket['total_ticket'] - ($ticket['total_ticket'] * .16), 2));
                $factura->iva_factura = str_replace(",", "", number_format(($ticket['total_ticket'] - ($ticket['total_ticket'] * .16)) * .16, 2));
                $factura->total_factura = str_replace(",", "", number_format($ticket['total_ticket'], 2));
                $factura->XML = $xml3;
                $factura->PDF = base64_encode($contenido);
                $factura->estatus = "timbrada";
                $factura->id_ticketAF = $ticket['id'];
                $factura->fecha_timbrado = date("Y-m-d H:i:s");
                $factura->uuid = $uuid;
                $factura->save();
                Alert::success('Valida', 'Comprobante timbrado');
                //unlink($est[0]['serie'] . ($est[0]['folio'] + 1) . '.xml');
                unlink($archivo);
                unlink($pdf1);
                unlink($ubiQr);
                return $this->sin_est();
                //*/
            } else {
                /*;
                return $this->index();*/
                $val2 = json_decode($response);
                if ($val2->data->count === 1) {
                    //return var_dump($val2->data->details[0]);
                    Alert::error('[' . $val2->data->details[0]->code . '] ' . $val2->data->details[0]->description, $val2->data->details[0]->validation);
                    return $this->sin_est();
                }
                return $response;
            }
        }
        /* */
    }

    public function rechazo2()
    {
        $coment = request()->validate([
            'id' => 'required',
            'motivo' => 'required',
        ]);
        $ticket = AutofTicket::find($coment['id']);
        if ($ticket->id >= 1) {
            $ticket->estatus = 'Rechazo';
            $ticket->coment = $coment['motivo'];
            $ticket->save();
            // Se manda correo
            $mail = new PHPMailer(true);
            $mail->From = "no-reply@centralmx.tech";
            $mail->FromName = "Central operadora de estacionamientos";
            $mail->Subject = "Rechazo Factura";
            $mail->Body = "<html><h4 style='font-color: green;' align='center';>Central Operadora de Estacionamientos SAPI S.A. de C.V.</h4>
 
<p align='center;'>Esta rechazando su factura por " . $coment['motivo'] . "</p>

<p align='center;'>Este correo electrónico ha sido generado automáticamente por el Sistema de Emisión de Comprobantes Digitales por lo que le solicitamos no responder a este mensaje, ya que las respuestas a este correo electrónico no serán leídas. En caso de tener alguna duda referente a la información contenida en el Comprobante Fiscal Digital contacte a Central Operadora de Estacionamientos SAPI S.A. de C.V. para su aclaración.</p>

<p align='center;'>Está recibiendo este correo electrónico debido a que ha proporcionado la dirección de correo electrónico " . $ticket->email . " a Central Operadora de Estacionamientos SAPI S.A. de C.V. para hacerle llegar su Factura Electrónica.!</p></html>";
            $mail->AddAddress($ticket->email, $ticket->Razon_social);
            $mail->IsHTML(true);
            $mail->Send();
            Alert::success('Rechazo', 'Se rechazo correctamente el ticket');
        } else {
            Alert::error('Error', 'No se pudo rechazar el ticket');
        }
        return $this->sin_est();
    }

}
