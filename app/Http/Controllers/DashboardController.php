<?php

namespace App\Http\Controllers;

use App\AutoFact;
use App\AutofTicket;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $hoy = date('Y-m-d');
        $total = DB::table('autof_tickets')
            ->count();
        $fact = AutofTicket::all()
            ->where('estatus', '=', 'valido')
            ->count();
        $facturado = DB::table('auto_facts')
            ->sum('total_factura');
        $rechazo = AutofTicket::all()->where('estatus', '=', 'Rechazo')->count();
        $TotalGraf = $total - $rechazo - $fact;

        $est = DB::table('autof_tickets')
            ->join('parks', 'autof_tickets.id_est', '=', 'parks.no_est')
            ->select('parks.nombre', DB::raw('count(*) as fact'))
            ->where('estatus', '=', 'valido')
            ->groupBy('parks.nombre')
            ->orderBy('fact', 'desc')
            ->take(7)
            ->get();
        return view('dashboard')
            ->with('total', $total)
            ->with('fact', $fact)
            ->with('facturado', $facturado)
            ->with('rechazo', $rechazo)
            ->with('TotalGraf', $TotalGraf)
            ->with('est', $est);
        //*/
    }
}
