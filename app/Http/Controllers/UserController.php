<?php

namespace App\Http\Controllers;

use App\CentralUser;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function list(){
        $user = CentralUser::withTrashed()->get();
        return view('Usuarios.lista',compact('user'));
    }

    public function reg(){
        return view('auth.register');
    }
    public function registro()
    {
        $coment = request()->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
        CentralUser::create([
            'name' => $coment['name'],
            'email' => $coment['email'],
            'password' => Hash::make($coment['password']),
        ]);
        Alert::success('Exito', 'Usuario Registrado');
        return $this->list();
    }

    public function baja(CentralUser $user)
    {
        $user->delete();
        Alert::success('Bloqueado', 'Usuario eliminado correctamente');
        return $this->list();
    }

    public function cura($user){
        CentralUser::onlyTrashed()->find($user)->restore();
        Alert::success('Restaurado', 'Usuario restaurado correctamente');
        return $this->list();
    }

    public function actualiza(){
        $pass = request()->validate([
            'user' => ['numeric','required'],
            'pass'=> ['required']
        ]);
        $user = CentralUser::find($pass['user']);
        $user->password = Hash::make($pass['pass']);
        $user->save();
        return $this->list();
    }
}
