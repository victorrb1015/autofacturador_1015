<?php

namespace App\Http\Controllers;

use App\Pension;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PensionController extends Controller
{
    public function index()
    {
        $p = DB::table('pensions')
            ->join('users', 'users.id', '=', 'pensions.id_cliente')
            ->join('tipo_pen', 'tipo_pen.id', '=', 'pensions.id_tipo_pen')
            ->join('tipo_pago', 'tipo_pago.id', '=', 'pensions.id_tipo_pago')
            ->join('fecha_limite', 'fecha_limite.id', '=', 'pensions.id_fecha_limite')
            ->join('forma_pago', 'forma_pago.id', '=', 'pensions.id_forma_pago')
            ->select('pensions.id',
                'pensions.num_pen',
                'pensions.no_est',
                'pensions.factura',
                'pensions.costo_pension',
                'pensions.recargos',
                'pensions.venta_tarjeta',
                'pensions.repo_tarjeta',
                'pensions.impor_pago',
                'pensions.mes_pago',
                'pensions.cargado_por',
                'users.name',
                'users.email',
                'tipo_pen.tipo as tipo_pen',
                'tipo_pago.tipo as tipo_pago',
                'fecha_limite.fecha',
                'forma_pago.forma'
            )
            ->get();
        return view('pension.pension')->with('p', $p);
    }
}
