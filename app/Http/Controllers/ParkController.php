<?php

namespace App\Http\Controllers;

use App\AutoFact;
use App\AutofTicket;
use App\Org;
use Illuminate\Http\Request;
use App\Park;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;

class ParkController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $est = Park::paginate(20);
        return view('estacionamiento.estacionamiento', compact('est'));
    }

    public function show(Park $no_est)
    {
        $org = Org::find($no_est->id_org);
        return view('estacionamiento.show')
            ->with('no_est', $no_est)
            ->with('org', $org);
    }

    public function fact(Park $no_est)
    {
        $rfc = DB::table('autof_tickets')
            ->select('RFC',DB::raw('count(*) as fact'))
            ->where('estatus','=','valido')
            ->where('id_est','=',$no_est->no_est)
            ->groupBy('RFC')
            ->orderBy('fact','desc')
            ->take(5)
            ->get();
        $hoy = date('Y-m-d');
        $total = DB::table('autof_tickets')
            //->whereDate('fecha_emision','=',$hoy)
            ->where('id_est','=',$no_est->no_est)
            ->count();
        $fact = AutofTicket::all()
            ->where('estatus','=','valido')
            ->where('id_est','=',$no_est->no_est)
            ->count();
        $facturado = DB::table('autof_tickets')
            ->join('parks','autof_tickets.id_est','=','parks.no_est')
            ->where('autof_tickets.estatus','=','valido')
            ->where('parks.no_est','=',$no_est->no_est)
            ->sum('total_ticket');
        //->get();
            //->sum('auto_facts.total_factura');

        $rechazo = AutofTicket::all()
            ->where('estatus','=','Rechazo')
            ->where('id_est','=',$no_est->no_est)
            ->count();
        $TotalGraf = $total-$rechazo-$fact;
        $ticket = AutofTicket::where('id_est','=',$no_est->no_est)->get();
        return view('estacionamiento.fact')
            ->with('tickets',$ticket)
            ->with('rfc',$rfc)
            ->with('total',$total)
            ->with('fact', $fact)
            ->with('facturado',$facturado)
            ->with('rechazo',$rechazo)
            ->with('TotalGraf',$TotalGraf); //*/
    }

    public function new()
    {
        $org = Org::all();
        return view('estacionamiento.nuevo', compact('org'));
    }

    public function nuevo()
    {
        $res = request()->validate([
            'park' => ['required', 'numeric'],
            'N_est' => ['required', 'string'],
            'org' => ['required'],
            'calle' => ['required', 'string'],
            'no_ext' => ['required', 'string'],
            'no_int' => [''],
            'cp' => ['required', 'string'],
            'colonia' => ['required', 'string'],
            'municipio' => ['required', 'string'],
            'estado' => ['required', 'string'],
            'tipo' => ['required'],
            'fact' => ['required'],
            'marca' => ['required', 'string'],
            'dist_regio' => ['required', 'string']
        ]);
        $park = new Park();
        $park->no_est = $res['park'];
        $park->nombre = $res['N_est'];
        $park->dist_regio = $res['dist_regio'];
        $park->calle = $res['calle'];
        $park->no_ext = $res['no_ext'];
        $park->no_int = $res['no_int'];
        $park->colonia = $res['colonia'];
        $park->municipio = $res['municipio'];
        $park->estado = $res['estado'];
        $park->cp = $res['cp'];
        $park->marca = $res['marca'];
        if ($res['tipo'] = "on") {
            $park->Automatico = true;
        } else {
            $park->Automatico = false;
        }
        if ($res['fact'] = "on") {
            $park->Facturable = true;
        } else {
            $park->Facturable = false;
        }
        $park->id_org = $res['org'];
        $park->serie = "J".$res['park'];
        $park->save();
        Alert::success('Creado', 'Se registro correctamente el estacionamiento');
        $est = Park::paginate(20);
        return view('estacionamiento.estacionamiento', compact('est'));
    }
}
