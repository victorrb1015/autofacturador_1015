<?php


Route::get('/', 'HomeController@index')->name('home');
//---DashboardController---
Route::get('/dashboard','DashboardController@index')->name('dashboard');
//---FacturasController---
Route::get('/facturas','FacturasController@index')->name('facturas');
Route::get('/nueva', 'FacturasController@nueva')->name('facturas.new');
Route::post('/generar', 'FacturasController@fact')->name('facturas.generar');
Route::post('/generar/crear', 'FacturasController@timb')->name('facturas.crear');
Route::get('/facturas/reenviar/{fact}','FacturasController@reenviar')->name('reenvio');

Route::get('/log','FacturasController@log')->name('log');
//---ParkController---
Route::get('/estacionamiento','ParkController@index')->name('park');
Route::get('/estacionamiento/nuevo','ParkController@new')->name('park.new');
Route::post('/estacionamiento/new','ParkController@nuevo')->name('park.nuevo');
Route::get('/estacionamiento/{no_est}','ParkController@show')->name('numero');
Route::get('/estacionamiento/fact/{no_est}','ParkController@fact')->name('park.fact');
//--TicketsController--
Route::get('/tickets','TicketsController@ticket')->name('tickets');
Route::get('/manual','TicketsController@manual')->name('man');
Route::get('/auto','TicketsController@auto')->name('auto');
Route::get('/validar','TicketsController@validar')->name('validar');
Route::post('/rechazo','TicketsController@rechazo')->name('rechazo');
Route::get('/valido/{ticket}','TicketsController@valido')->name('ticket.valido');
Route::post('/actualiza','TicketsController@actualiza_estacionamiento1')->name('actualiza');
Route::post('/actualiza/estacionamiento','TicketsController@actualiza_estacionamiento2')->name('actualiza2');
Route::get('/validacion/estacionamiento','TicketsController@sin_est')->name('sin_est');
Route::get('/valido/estacionamiento/{ticket}','TicketsController@valido2')->name('ticket.valido2');
Route::post('/rechazo/estacionamiento','TicketsController@rechazo2')->name('ticket.rechazo2');
Route::get('/rechazada','TicketsController@rechazada')->name('rechazada');
//UserController
Auth::routes();
Route::get('/registro','UserController@reg')->name('reg');
Route::get('/list','UserController@list')->name('lista');
Route::post('/registro/usuario','UserController@registro')->name('reg.new');
Route::get('/list/{user}','UserController@baja')->name('baja');
Route::get('/lista/borrados/{user}','UserController@cura')->name('cura');
Route::post('/list/update','UserController@actualiza')->name('update');
//PensionController
Route::get('/pensiones','PensionController@index')->name('pen');
