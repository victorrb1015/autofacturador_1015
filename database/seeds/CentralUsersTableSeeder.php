<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class CentralUsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('central_users')->insert([
            'name'=>'Victor Rojas',
            'email'=> 'vrojas@integrador-technology.mx',
            'password'=> Hash::make('123456789'),
        ]);
        DB::table('central_users')->insert([
            'name'=>'John Vargas',
            'email'=> 'lfernando@integrador-technology.mx',
            'password'=> Hash::make('Administrador1'),
        ]);
        DB::table('central_users')->insert([
            'name'=>'John Vargas',
            'email'=> 'gerente@central-mx.com',
            'password'=> Hash::make('123456789'),
        ]);
    }
}
