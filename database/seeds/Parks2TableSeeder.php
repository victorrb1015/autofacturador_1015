<?php

use Illuminate\Database\Seeder;

class Parks2TableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('parks')->insert([
            'no_est' => 275,
            'nombre' => 'METROPOLI PATRIOTISMO',
            'dist_regio' => 'DF3',
            'calle' => 'AV PATRIOTISMO',
            'no_ext' => '229',
            'colonia' => 'SAN PEDRO DE LOS PINOS',
            'municipio' => 'BENITO JUAREZ',
            'estado' => 'CIUDAD DE MEXICO',
            'cp' => '03800',
            'Facturable' => true,
            'marca' => 'VENTUM',
            'Automatico' => true,
            'id_org' => 1,
            'serie' => 'J275',
        ]);
        
    }
}
