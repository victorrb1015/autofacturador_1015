<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(OrgsTableSeeder::class);
        $this->call(ParksTableSeeder::class);
        //$this->call(UserTableSeeder::class);
        $this->call(CentralUsersTableSeeder::class);
        $this->call(TempParksTableSeeder::class);
        $this->call(TipoTableSeeder::class);
        $this->call(Parks2TableSeeder::class);
        $this->call(TiposTableSeeder::class);
    }
}
