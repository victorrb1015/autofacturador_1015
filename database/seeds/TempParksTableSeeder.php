<?php

use Illuminate\Database\Seeder;

class TempParksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('park_temps')->insert([
            'fecha'=> '2019-05-19 14:38:32',
            'folio' => '008021837944',
            'monto' => '60.0',
            'park' => 8
        ]);
        DB::table('park_temps')->insert([
        'fecha'=> '2019-05-19 14:38:32',
        'folio' => '008021837945',
        'monto' => '60.0',
        'park' => 8
        ]);
        DB::table('park_temps')->insert([
            'fecha'=> '2019-05-19 14:38:32',
            'folio' => '008021837946',
            'monto' => '60.0',
            'park' => 8
        ]);
    }
}
