<?php

use Illuminate\Database\Seeder;

class TipoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ticket_types')->insert([
            'tipo' => 'Manual',
        ]);
        DB::table('ticket_types')->insert([
            'tipo' => 'Automatico',
        ]);
    }
}
