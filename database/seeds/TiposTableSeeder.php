<?php

use Illuminate\Database\Seeder;

class TiposTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipo_pen')->insert([
            'tipo' => 'Locatario'
        ]);
        DB::table('tipo_pen')->insert([
            'tipo' => 'Externo'
        ]);

        DB::table('tipo_pago')->insert([
            'tipo' => 'Mensual'
        ]);
        DB::table('tipo_pago')->insert([
            'tipo' => 'Trimestral'
        ]);
        DB::table('tipo_pago')->insert([
            'tipo' => 'Semestral'
        ]);
        DB::table('tipo_pago')->insert([
            'tipo' => 'Anual'
        ]);

        DB::table('forma_pago')->insert([
            'forma' => 'Transferencia'
        ]);
        DB::table('forma_pago')->insert([
            'forma' => 'Deposito'
        ]);

        DB::table('fecha_limite')->insert(['fecha' => 'dia 11 de cada mes']);
        DB::table('fecha_limite')->insert(['fecha' => 'dia 12 de cada mes']);
        DB::table('fecha_limite')->insert(['fecha' => 'dia 13 de cada mes']);
        DB::table('fecha_limite')->insert(['fecha' => 'dia 14 de cada mes']);
        DB::table('fecha_limite')->insert(['fecha' => 'dia 15 de cada mes']);
        DB::table('fecha_limite')->insert(['fecha' => 'dia 16 de cada mes']);
        DB::table('fecha_limite')->insert(['fecha' => 'dia 17 de cada mes']);
        DB::table('fecha_limite')->insert(['fecha' => 'dia 18 de cada mes']);
        DB::table('fecha_limite')->insert(['fecha' => 'dia 19 de cada mes']);
        DB::table('fecha_limite')->insert(['fecha' => 'dia 20 de cada mes']);
        DB::table('fecha_limite')->insert(['fecha' => 'dia 21 de cada mes']);
        DB::table('fecha_limite')->insert(['fecha' => 'dia 22 de cada mes']);
        DB::table('fecha_limite')->insert(['fecha' => 'dia 23 de cada mes']);
        DB::table('fecha_limite')->insert(['fecha' => 'dia 24 de cada mes']);
        DB::table('fecha_limite')->insert(['fecha' => 'dia 25 de cada mes']);
        DB::table('fecha_limite')->insert(['fecha' => 'dia 26 de cada mes']);
        DB::table('fecha_limite')->insert(['fecha' => 'dia 27 de cada mes']);
        DB::table('fecha_limite')->insert(['fecha' => 'dia 28 de cada mes']);
        DB::table('fecha_limite')->insert(['fecha' => 'dia 29 de cada mes']);
        DB::table('fecha_limite')->insert(['fecha' => 'dia 30 de cada mes']);
        DB::table('fecha_limite')->insert(['fecha' => 'dia 31 de cada mes']);

    }
}
