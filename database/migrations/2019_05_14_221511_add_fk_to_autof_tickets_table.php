<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkToAutofTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('autof_tickets', function (Blueprint $table) {
            $table->unsignedBigInteger('id_cliente');
            $table->integer('id_est');
            $table->unsignedBigInteger('id_tipo');
            $table->unsignedBigInteger('id_CentralUser');
            $table->foreign('id_cliente')
                ->references('id')->on('users')
                ->onDelete('cascade');
            $table->foreign('id_est')
                ->references('no_est')->on('parks')
                ->onDelete('cascade');
            $table->foreign('id_tipo')
                ->references('id')->on('ticket_types')
                ->onDelete('cascade');
            $table->foreign('id_CentralUser')
                ->references('id')->on('central_users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('autof_tickets', function (Blueprint $table) {
            $table->dropForeign(['id_cliente','id_est','id_tipo','id_pago','id_UsoCFDI','id_CentralUser']);
        });
    }
}
