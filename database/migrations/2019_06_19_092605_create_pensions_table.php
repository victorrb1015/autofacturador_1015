<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePensionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pensions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('num_pen');
            $table->longText('foto_comprobante')->nullable();
            $table->integer('no_est');
            $table->boolean('factura');
            $table->decimal('costo_pension');
            $table->decimal('recargos');
            $table->decimal('venta_tarjeta');
            $table->decimal('repo_tarjeta');
            $table->decimal('impor_pago');
            $table->integer('mes_pago');
            $table->string('cargado_por');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pensions');
    }
}
