<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parks', function (Blueprint $table) {
            $table->integer('no_est')->primary();
            $table->string('nombre');
            $table->string('dist_regio')->nullable();
            $table->string('calle')->nullable();
            $table->string('no_ext')->nullable();
            $table->string('no_int')->nullable();
            $table->string('colonia')->nullable();
            $table->string('municipio')->nullable();
            $table->string('estado')->nullable();
            $table->string('cp');
            $table->boolean('Facturable');
            $table->string('marca');
            $table->boolean('Automatico');
            $table->integer('cajones')->nullable();
            $table->integer('folio')->default(0);
            $table->string('serie');
            $table->string('correo')->nullable();
            $table->integer('pensiones')->default(0);
            $table->integer('cortesias')->default(0);
            $table->integer('cobro')->default(0);
            $table->string('observaciones')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parks');
    }
}
