<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkPension2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pensions', function (Blueprint $table) {
            $table->unsignedBigInteger('id_tipo_pen');
            $table->unsignedBigInteger('id_tipo_pago');
            $table->unsignedBigInteger('id_forma_pago');
            $table->unsignedBigInteger('id_fecha_limite');
            $table->foreign('id_tipo_pen')
                ->references('id')->on('tipo_pen')
                ->onDelete('cascade');
            $table->foreign('id_tipo_pago')
                ->references('id')->on('tipo_pago')
                ->onDelete('cascade');
            $table->foreign('id_forma_pago')
                ->references('id')->on('forma_pago')
                ->onDelete('cascade');
            $table->foreign('id_fecha_limite')
                ->references('id')->on('fecha_limite')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pensions', function (Blueprint $table) {
            $table->dropForeign(['id_tipo_pen','id_tipo_pago','id_forma_pago','id_fecha_limite']);
        });
    }
}
