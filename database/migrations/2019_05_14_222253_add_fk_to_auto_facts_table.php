<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkToAutoFactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('auto_facts', function (Blueprint $table) {
            $table->unsignedBigInteger('id_ticketAF');
            $table->foreign('id_ticketAF')
                ->references('id')->on('autof_tickets')
                ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('auto_facts', function (Blueprint $table) {
            $table->dropForeign(['id_ticketAF']);
        });
    }
}
